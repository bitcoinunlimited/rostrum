use std::sync::Arc;

use crate::{
    chaindef::{ScriptHash, TokenID},
    indexes::{outputindex::OutputIndexRow, unspentindex::UnspentIndexRow, DBRow},
    query::{
        queryutil::{multi_get_utxo, outpoints_are_spent, token_from_outpoint},
        BUFFER_SIZE, CHUNK_SIZE,
    },
    store::DBStore,
};

use super::queryfilter::QueryFilter;

use anyhow::Result;
use bitcoin_hashes::hex::ToHex;
use futures::{channel::mpsc::Sender, stream, try_join, StreamExt};
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};

pub type UtxoEntry = (
    UnspentIndexRow,
    OutputIndexRow,
    Option<(TokenID, i64, Option<Vec<u8>>)>,
);

/// Retrive utxos in db, that are not spent in (optional) filter_db.
pub async fn get_utxos_in_db(
    db: &Arc<DBStore>,
    filter_db: Option<&Arc<DBStore>>,
    scripthash: &ScriptHash,
    filter: &QueryFilter,
    filter_token: &Option<TokenID>,
    sender: Sender<UtxoEntry>,
) -> Result<()> {
    let (query, stream) = db
        .scan(
            UnspentIndexRow::CF,
            UnspentIndexRow::scripthash_filter(scripthash),
        )
        .await;
    let utxos: Vec<UnspentIndexRow> = stream
        .ready_chunks(CHUNK_SIZE)
        .flat_map_unordered(BUFFER_SIZE, move |utxos| {
            let utxos: Vec<UnspentIndexRow> = utxos.iter().map(UnspentIndexRow::from_row).collect();
            let utxos = if filter.token_only {
                utxos.into_iter().filter(|u| u.has_token()).collect()
            } else if filter.exclude_tokens {
                utxos.into_iter().filter(|u| !u.has_token()).collect()
            } else {
                utxos
            };
            stream::iter(utxos.into_iter())
        })
        .collect()
        .await;
    query.await??;

    // filter out any spends in mempool
    let utxos: Vec<UnspentIndexRow> = if let Some(filter_db) = filter_db {
        stream::iter(utxos.into_iter())
            .ready_chunks(CHUNK_SIZE)
            .then(|utxos| {
                let filter_db = filter_db.clone();
                async move {
                    let outpoints = utxos
                        .par_iter()
                        .map(|u| u.outpointhash())
                        .collect::<Vec<_>>();
                    let spends = outpoints_are_spent(&filter_db, &outpoints).await;
                    let filtered = utxos
                        .into_iter()
                        .zip(spends.into_iter())
                        .filter_map(|(o, spent)| if spent { None } else { Some(o) })
                        .collect::<Vec<_>>();
                    stream::iter(filtered.into_iter())
                }
            })
            .flatten_unordered(BUFFER_SIZE)
            .collect()
            .await
    } else {
        utxos
    };

    // collect token data and filter out tokens if `filter_token` is set
    let utxos = if filter.exclude_tokens {
        utxos.into_iter().map(|u| (u, None)).collect::<Vec<_>>()
    } else {
        let with_token: Vec<(UnspentIndexRow, Option<_>)> = stream::iter(utxos.into_iter())
            .filter_map(|utxo| {
                let store = db.clone();

                async move {
                    let token = token_from_outpoint(&store, &utxo.outpointhash())
                        .await
                        .expect("failed to fetch token");
                    if let (Some(ft), Some((t, _, _))) = (filter_token, token.as_ref()) {
                        if t != ft {
                            return None;
                        }
                    };

                    Some((utxo, token))
                }
            })
            .collect()
            .await;
        with_token
    };

    // finally; with all filtering done; fetch remaining utxo information
    stream::iter(utxos.into_iter())
        .ready_chunks(CHUNK_SIZE)
        .for_each_concurrent(BUFFER_SIZE, |unspent_with_token| {
            let store = db.clone();
            let mut sender = sender.clone();

            async move {
                let outpoints = unspent_with_token
                    .iter()
                    .map(|(unspent, _)| unspent.outpointhash())
                    .collect::<Vec<_>>();
                let utxos = multi_get_utxo(&store, &outpoints).await;

                unspent_with_token
                    .into_iter()
                    .zip(utxos.into_iter())
                    .for_each(|((unspent, token), utxo)| {
                        let utxo = match utxo {
                            Some(u) => u,
                            None => {
                                warn!(
                                    "unspent: did not find utxo {}",
                                    unspent.outpointhash().to_hex()
                                );
                                return;
                            }
                        };

                        sender
                            .start_send((unspent, utxo, token))
                            .expect("failed to send utxo");
                    });
            }
        })
        .await;

    Ok(())
}

/// Use the unspent index to fetch utxos matching filter at chaintip.
pub async fn get_utxos_at_tip(
    confirmed: &Arc<DBStore>,
    mempool: &Arc<DBStore>,
    scripthash: &ScriptHash,
    filter: &QueryFilter,
    filter_token: &Option<TokenID>,
    sender: Sender<UtxoEntry>,
) -> Result<()>
where
{
    assert!(
        !filter.has_height_filter(),
        "unspent index does not have historical info"
    );

    try_join!(
        get_utxos_in_db(
            confirmed,
            Some(mempool),
            scripthash,
            filter,
            filter_token,
            sender.clone()
        ),
        get_utxos_in_db(mempool, None, scripthash, filter, filter_token, sender)
    )?;

    Ok(())
}
