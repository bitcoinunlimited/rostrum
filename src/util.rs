use tokio::sync::{
    mpsc::{channel, Receiver, Sender},
    Mutex, MutexGuard,
};

pub(crate) type Bytes = Box<[u8]>;

pub struct Channel<T> {
    tx: Sender<T>,
    rx: Mutex<Receiver<T>>,
}

impl<T> Channel<T> {
    pub fn bounded(capacity: usize) -> Self {
        let (tx, rx) = channel(capacity);
        Channel {
            tx,
            rx: Mutex::new(rx),
        }
    }

    pub fn sender(&self) -> Sender<T> {
        self.tx.clone()
    }

    pub async fn receiver(&self) -> MutexGuard<Receiver<T>> {
        self.rx.lock().await
    }
}
