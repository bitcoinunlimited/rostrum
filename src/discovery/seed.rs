// Known good servers to seed the server discovery.

use super::peer::CandidatePeer;

// Known good seed peers
#[cfg(nexa)]
pub const SEED_PEERS_MAINNET: &[CandidatePeer] = &[
    // rostrum.nexa.ink
    CandidatePeer {
        tcp_port: Some(20001),
        ssl_port: Some(20002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(3, 13, 182, 215)),
        version: None,
    },
    // rostrum.nexa.org
    CandidatePeer {
        tcp_port: Some(20001),
        ssl_port: Some(20002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(178, 62, 35, 191)),
        version: None,
    },
];

#[cfg(nexa)]
pub const SEED_PEERS_TESTNET: &[CandidatePeer] = &[];

// Testnet4 is BCH only
#[cfg(nexa)]
pub const SEED_PEERS_TESTNET4: &[CandidatePeer] = &[];

// Chipnet is BCH only
#[cfg(nexa)]
pub const SEED_PEERS_CHIPNET: &[CandidatePeer] = &[];

// Known good seed peers
#[cfg(bch)]
pub const SEED_PEERS_MAINNET: &[CandidatePeer] = &[
    // bitcoincash.network
    CandidatePeer {
        tcp_port: Some(50001),
        ssl_port: Some(50002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(3, 13, 182, 215)),
        version: None,
    },
    // electrs.bitcoinunlimited.info
    CandidatePeer {
        tcp_port: Some(50001),
        ssl_port: Some(50002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(101, 36, 125, 103)),
        version: None,
    },
    // bch.loping.net
    CandidatePeer {
        tcp_port: Some(50001),
        ssl_port: Some(50002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(180, 183, 226, 37)),
        version: None,
    },
    // bch.imaginary.cash
    CandidatePeer {
        tcp_port: Some(50001),
        ssl_port: Some(50002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(67, 223, 119, 97)),
        version: None,
    },
    // blackie.c3-soft.com
    CandidatePeer {
        tcp_port: Some(50001),
        ssl_port: Some(50002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(208, 88, 16, 38)),
        version: None,
    },
];

#[cfg(bch)]
pub const SEED_PEERS_TESTNET: &[CandidatePeer] = &[
    // testnet.bitcoincash.network
    CandidatePeer {
        tcp_port: Some(60001),
        ssl_port: Some(60002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(3, 13, 182, 215)),
        version: None,
    },
    // tbch.loping.net
    CandidatePeer {
        tcp_port: Some(60001),
        ssl_port: Some(60002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(27, 130, 255, 115)),
        version: None,
    },
    // blackie.c3-soft.com
    CandidatePeer {
        tcp_port: Some(60001),
        ssl_port: Some(60002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(208, 88, 16, 38)),
        version: None,
    },
    // electroncash.de
    CandidatePeer {
        tcp_port: Some(50003),
        ssl_port: Some(60004),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(193, 135, 10, 219)),
        version: None,
    },
    // bch0.kister.net
    CandidatePeer {
        tcp_port: Some(51001),
        ssl_port: Some(51002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(100, 11, 85, 230)),
        version: None,
    },
];

#[cfg(bch)]
pub const SEED_PEERS_TESTNET4: &[CandidatePeer] = &[
    // testnet.bitcoincash.network
    CandidatePeer {
        tcp_port: Some(62001),
        ssl_port: Some(62002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(3, 13, 182, 215)),
        version: None,
    },
    // tbch4.loping.net
    CandidatePeer {
        tcp_port: Some(62001),
        ssl_port: Some(62002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(27, 130, 255, 115)),
        version: None,
    },
    // blackie.c3-soft.com
    CandidatePeer {
        tcp_port: Some(62001),
        ssl_port: Some(62002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(208, 88, 16, 38)),
        version: None,
    },
    // "electroncash.de"
    CandidatePeer {
        tcp_port: Some(54003),
        ssl_port: Some(54004),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(100, 64, 0, 7)),
        version: None,
    },
];

#[cfg(bch)]
pub const SEED_PEERS_CHIPNET: &[CandidatePeer] = &[
    // cbch.loping.net
    CandidatePeer {
        tcp_port: Some(62101),
        ssl_port: Some(62102),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(223, 204, 69, 11)),
        version: None,
    },
    // chipnet.c3-soft.com
    CandidatePeer {
        tcp_port: Some(64001),
        ssl_port: Some(64002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(208, 88, 16, 38)),
        version: None,
    },
    // electroncash.de
    CandidatePeer {
        tcp_port: Some(55003),
        ssl_port: Some(55004),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(100, 64, 0, 7)),
        version: None,
    },
    // chipnet.imaginary.cash
    CandidatePeer {
        tcp_port: Some(50001),
        ssl_port: Some(50002),
        ws_port: None,
        wss_port: None,
        hostname: None,
        ip: std::net::IpAddr::V4(std::net::Ipv4Addr::new(66, 29, 152, 199)),
        version: None,
    },
];
