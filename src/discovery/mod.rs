pub mod announcer;
pub mod client;
pub mod discoverer;
pub mod peer;
pub mod seed;
pub mod utilparser;

// If a version for a candidate is not provided, assume this verison.
pub const FALLBACK_VERSION: &str = "1.4";
