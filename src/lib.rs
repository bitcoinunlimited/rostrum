#![recursion_limit = "1024"]
#![cfg_attr(feature = "fail-on-warnings", deny(warnings))]
#![forbid(unsafe_code)]
// BCH BlockHeader is copyable, while Nexa isn't.
// Disable this linter for BCH.
#![cfg_attr(not(feature = "nexa"), allow(clippy::clone_on_copy))]
#![cfg_attr(feature = "with-benchmarks", feature(test))]

#[cfg(feature = "with-benchmarks")]
extern crate test;

#[macro_use]
extern crate anyhow;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_json;
extern crate serde;
#[macro_use]
extern crate configure_me;

#[cfg(target_os = "linux")]
use tikv_jemallocator::Jemalloc;

#[cfg(target_os = "linux")]
#[global_allocator]
static GLOBAL: Jemalloc = Jemalloc;

#[macro_use]
extern crate bitcoin_hashes;

pub mod app;
pub mod bch;
pub mod cache;
pub mod chain;
pub mod chaindef;
pub mod config;
pub mod daemon;
pub mod def;
pub mod discovery;
pub mod doslimit;
pub mod encode;
pub mod errors;
pub mod index;
pub mod indexes;
pub mod mempool;
pub mod metrics;
pub mod nexa;
pub mod query;
pub mod rndcache;
pub mod rpc;
pub mod scripthash;
pub mod signal;
pub mod store;
pub mod thread;
pub mod timeout;
pub mod util;
pub mod utilnumber;
pub mod utilscript;
pub mod utilserialize;
pub mod writebatch;
