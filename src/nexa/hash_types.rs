use crate::impl_nexa_hashencode;
use bitcoin_hashes::sha256;
use bitcoin_hashes::sha256d;

hash_newtype!(
    BlockHash,
    sha256::Hash,
    32,
    doc = "A nexa block hash.",
    true
);
hash_newtype!(
    TxIdem,
    sha256d::Hash,
    32,
    doc = "A bitcoin transaction hash/transaction idem."
);

impl_nexa_hashencode!(BlockHash);
impl_nexa_hashencode!(TxIdem);
