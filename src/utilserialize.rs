#[cfg(bch)]
use bitcoin_hashes::hex::ToHex;

use crate::mempool::MEMPOOL_HEIGHT;

#[cfg(bch)]
pub(crate) fn as_hex<T, S>(buffer: &T, serializer: S) -> Result<S::Ok, S::Error>
where
    T: AsRef<[u8]>,
    S: serde::Serializer,
{
    serializer.serialize_str(&buffer.as_ref().to_hex())
}

#[cfg(bch)]
pub(crate) fn opt_as_hex<S>(key: &Option<Vec<u8>>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    match key {
        Some(bytes) => serializer.serialize_str(&hex::encode(bytes)),
        None => serializer.serialize_none(),
    }
}

pub(crate) fn as_rpc_height<S>(key: &u32, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    if key == &MEMPOOL_HEIGHT {
        serializer.serialize_i64(0)
    } else {
        serializer.serialize_i64((*key).into())
    }
}
