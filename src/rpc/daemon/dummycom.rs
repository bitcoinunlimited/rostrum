use async_trait::async_trait;
use tokio::sync::mpsc::Sender;

use super::{communication::Communcation, Message};
use anyhow::Result;
use serde_json::Value;

pub struct DummyCom {}

#[async_trait]
impl Communcation for DummyCom {
    async fn send_values(&mut self, _values: &[Value]) -> Result<()> {
        Ok(())
    }

    fn start_request_receiver(&mut self, _rpc_queue_sender: Sender<Message>, _idle_timeout: u64) {}

    async fn shutdown(&mut self) {}
}
