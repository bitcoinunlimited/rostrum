use std::{net::SocketAddr, sync::Arc, time::Duration};

use anyhow::{Context, Result};
use async_trait::async_trait;
use serde_json::Value;
use tokio::{
    io::{AsyncBufReadExt, AsyncWriteExt, BufReader},
    net::{
        tcp::{OwnedReadHalf, OwnedWriteHalf},
        TcpStream,
    },
    sync::{mpsc::Sender, Mutex},
    time::timeout,
};

use crate::{
    config::Config,
    doslimit::{ConnectionLimits, GlobalLimits},
    query::Query,
    rpc::{daemon::connection::Connection, rpcstats::RpcStats},
    signal::{NetworkNotifier, Waiter},
    util::Channel,
};

use super::{communication::Communcation, Message};

pub struct TcpCommuncation {
    reader: Option<OwnedReadHalf>,
    writer: OwnedWriteHalf,
}

impl TcpCommuncation {
    pub fn new(stream: TcpStream) -> Self {
        let (reader, writer) = stream.into_split();

        Self {
            reader: Some(reader),
            writer,
        }
    }

    async fn parse_requests(
        mut reader: BufReader<OwnedReadHalf>,
        tx: Sender<Message>,
        idle_timeout: u64,
    ) -> Result<()> {
        loop {
            if Waiter::shutdown_check().is_err() {
                let _ = tx.send(Message::Done).await;
                bail!("shutdown");
            }
            let mut line = Vec::<u8>::new();

            // Setting a timeout for the read_until operation
            let read_result = timeout(
                Duration::from_secs(idle_timeout),
                reader.read_until(b'\n', &mut line),
            )
            .await;

            match read_result {
                Ok(Ok(bytes_read)) => {
                    if bytes_read == 0 {
                        tx.send(Message::Done).await.context("channel closed")?;
                        return Ok(());
                    }
                    if line.starts_with(&[22, 3, 1]) {
                        // (very) naive SSL handshake detection
                        let _ = tx.send(Message::Done).await;
                        bail!("invalid request - maybe SSL-encrypted data?: {:?}", line)
                    }
                    match String::from_utf8(line) {
                        Ok(req) => tx
                            .send(Message::Request(req))
                            .await
                            .context("channel closed")?,
                        Err(err) => {
                            let _ = tx.send(Message::Done).await;
                            bail!("invalid UTF8: {}", err)
                        }
                    }
                }
                // Handling the timeout case
                Err(tokio::time::error::Elapsed { .. }) => {
                    let _ = tx.send(Message::Done).await;
                    bail!(
                        "idle client did not send a message for {} seconds",
                        idle_timeout
                    );
                }
                Ok(Err(e)) => {
                    let _ = tx.send(Message::Done).await;
                    bail!("failed to read a request: {}", e);
                }
            }
        }
    }
}

#[async_trait]
impl Communcation for TcpCommuncation {
    async fn send_values(&mut self, values: &[Value]) -> Result<()> {
        for value in values {
            let line = value.to_string() + "\n";
            if let Err(e) = self.writer.write_all(line.as_bytes()).await {
                let truncated: String = line.chars().take(80).collect();
                return Err(e).context(format!("failed to send {}", truncated));
            }
        }
        Ok(())
    }

    fn start_request_receiver(&mut self, rpc_queue_sender: Sender<Message>, idle_timeout: u64) {
        let reader = self.reader.take();

        crate::thread::spawn_task("peer_reader", async move {
            // Give the reader to this task
            let bufreader = BufReader::new(reader.unwrap());
            if let Err(e) = Self::parse_requests(bufreader, rpc_queue_sender, idle_timeout).await {
                trace!("peer_reader thread: {}", e);
            }
            Ok(())
        });
    }

    async fn shutdown(&mut self) {
        let _ = self.writer.shutdown().await;
    }
}

#[allow(clippy::too_many_arguments)]
pub async fn start_tcp_server(
    conn_acceptor: Channel<Option<(TcpStream, SocketAddr)>>,
    global_limits: Arc<GlobalLimits>,
    config: Arc<Config>,
    query: Arc<Query>,
    stats: Arc<RpcStats>,
    connection_limits: ConnectionLimits,
    network_notifier: Arc<NetworkNotifier>,
    rpc_queue_senders: Arc<Mutex<Vec<Sender<Message>>>>,
) -> Result<()> {
    while let Some(conn) = conn_acceptor.receiver().await.recv().await {
        let (mut stream, addr) = match conn {
            Some(c) => c,
            None => break,
        };

        let global_limits = global_limits.clone();

        let mut connections = match global_limits.inc_connection(&addr.ip()).await {
            Err(e) => {
                trace!("[{}] dropping tcp peer - {}", addr, e);
                let _ = stream.shutdown().await;
                continue;
            }
            Ok(n) => n,
        };
        // explicitely scope the shadowed variables for the new thread
        let query_cpy = Arc::clone(&query);
        let stats_cpy = Arc::clone(&stats);
        let network_notifier_cpy = Arc::clone(&network_notifier);
        let config = Arc::clone(&config);
        let rpc_queue = Channel::bounded(config.rpc_buffer_size);

        rpc_queue_senders.lock().await.push(rpc_queue.sender());

        crate::thread::spawn_task("tcp_peer", async move {
            if connections != (0, 0) {
                debug!(
                    "[{} tcp] connected peer ({:?} out of {:?} connection slots used)",
                    addr,
                    connections,
                    global_limits.connection_limits(),
                );
            }

            let communication = TcpCommuncation::new(stream);

            let conn = Connection::new(
                config,
                query_cpy,
                addr,
                stats_cpy,
                connection_limits,
                Box::new(communication),
                network_notifier_cpy,
            );
            conn.run(rpc_queue).await;
            match global_limits.dec_connection(&addr.ip()).await {
                Ok(n) => connections = n,
                Err(e) => error!("{}", e),
            };
            debug!(
                "[{} tcp] disconnected peer ({:?} out of {:?} connection slots used)",
                addr,
                connections,
                global_limits.connection_limits(),
            );
            Ok(())
        });
    }

    info!("TCP RPC connections no longer accepted");
    Ok(())
}
