use anyhow::{Context, Result};
use bitcoin_hashes::Hash;

use crate::chaindef::{OutPointHash, ScriptHash};
use crate::query::queryfilter::QueryFilter;
use crate::store::Row;

use super::DBRow;

const SCRIPTHASHINDEX_CF: &str = "scripthash";

/**
 * A flag (1 byte) to define type of output to allow efficent
 * filtering.
 */
#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum OutputFlags {
    /// "Normal" output.
    None = 0,
    /// Output contains token in it.
    HasTokens = 1,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct ScriptHashIndexKey {
    pub scripthash: [u8; 32],
    flags: u8,
    // Outpointhash needs to be part of key to make it unique
    outpointhash: [u8; 32],
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct ScriptHashIndexRow {
    key: ScriptHashIndexKey,

    // redundant, as it's in heightindex, but speeds up height filtering
    height: u32,
}

impl ScriptHashIndexRow {
    pub fn new(
        scripthash: &ScriptHash,
        outpointhash: &OutPointHash,
        flags: OutputFlags,
        height: u32,
    ) -> ScriptHashIndexRow {
        ScriptHashIndexRow {
            key: ScriptHashIndexKey {
                scripthash: scripthash.into_inner(),
                flags: flags as u8,
                outpointhash: outpointhash.into_inner(),
            },
            height,
        }
    }

    fn filter_include_all(scripthash: [u8; 32]) -> Vec<u8> {
        bincode::serialize(&scripthash).unwrap()
    }

    pub fn filter_by_scripthash(scripthash: [u8; 32], filter: &QueryFilter) -> Vec<u8> {
        if filter.token_only {
            return Self::filter_include_all(scripthash)
                .into_iter()
                .chain(std::iter::once(OutputFlags::HasTokens as u8))
                .collect();
        }

        if filter.exclude_tokens {
            return Self::filter_include_all(scripthash)
                .into_iter()
                .chain(std::iter::once(OutputFlags::None as u8))
                .collect();
        }

        Self::filter_include_all(scripthash)
    }

    pub fn outpointhash(&self) -> OutPointHash {
        OutPointHash::from_inner(self.key.outpointhash)
    }

    pub fn has_token(&self) -> bool {
        self.key.flags == OutputFlags::HasTokens as u8
    }

    pub fn get_height(&self) -> u32 {
        self.height
    }
}

fn deserialize_value(value: &[u8]) -> Result<u32> {
    bincode::deserialize(value).context("failed to deserialize ScriptHashIndexRow value")
}

impl DBRow for ScriptHashIndexRow {
    const CF: &'static str = SCRIPTHASHINDEX_CF;

    fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self.key).unwrap().into_boxed_slice(),
            value: bincode::serialize(&self.height).unwrap().into_boxed_slice(),
        }
    }

    fn from_row(row: &Row) -> Self {
        let height = deserialize_value(&row.value).expect("failed to parse ScriptHashIndexRow");
        Self {
            key: bincode::deserialize(&row.key).expect("failed to read key for ScriptHashIndexRow"),
            height,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_to_from_row() {
        let row = ScriptHashIndexRow::new(
            &ScriptHash::hash(&[42]),
            &OutPointHash::hash(&[11]),
            OutputFlags::HasTokens,
            42,
        );
        let decoded_row = ScriptHashIndexRow::from_row(&row.to_row());
        assert_eq!(row, decoded_row);
    }

    #[test]
    fn test_filter_by_scripthash_with_token() {
        let scripthash = ScriptHash::hash(&[42]);
        let row_with_token = ScriptHashIndexRow::new(
            &scripthash,
            &OutPointHash::hash(&[11]),
            OutputFlags::HasTokens,
            42,
        )
        .to_row();
        let row_without_token = ScriptHashIndexRow::new(
            &scripthash,
            &OutPointHash::hash(&[11]),
            OutputFlags::None,
            42,
        )
        .to_row();

        let filter_token = ScriptHashIndexRow::filter_by_scripthash(
            scripthash.into_inner(),
            &QueryFilter::filter_token_only(),
        );
        let all = ScriptHashIndexRow::filter_by_scripthash(
            scripthash.into_inner(),
            &QueryFilter::default(),
        );
        let exclude_token = ScriptHashIndexRow::filter_by_scripthash(
            scripthash.into_inner(),
            &QueryFilter::filter_exclude_tokens(),
        );

        let starts_with = |a: &Row, b: &Vec<u8>| -> bool {
            a.key.to_vec().iter().zip(b.iter()).all(|(x, y)| x == y)
        };
        assert!(starts_with(&row_with_token, &filter_token));
        assert!(!starts_with(&row_without_token, &filter_token));

        assert!(starts_with(&row_with_token, &all));
        assert!(starts_with(&row_without_token, &all));

        assert!(!starts_with(&row_with_token, &exclude_token));
        assert!(starts_with(&row_without_token, &exclude_token));
    }
}
