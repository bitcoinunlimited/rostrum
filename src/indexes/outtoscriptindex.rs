use crate::chaindef::ScriptHash;

use crate::{chaindef::OutPointHash, store::Row};

use super::DBRow;
use crate::bitcoin_hashes::Hash;

const OUTTOSCRIPTHASH_CF: &str = "outtoscripthash";

// Used to find keys when pruning unspentindex.
// We never delete these rows as we need them in case of reorg.
// TODO: We can start pruning after ~100 blocks.

#[derive(Serialize, Deserialize, Debug)]
pub struct OutToScripthashIndex {
    pub outpointhash: [u8; 32], // key
    pub scripthash: [u8; 32],
}

impl OutToScripthashIndex {
    pub fn new(scripthash: ScriptHash, oph: OutPointHash) -> Self {
        Self {
            scripthash: scripthash.into_inner(),
            outpointhash: oph.into_inner(),
        }
    }

    pub fn to_key(oph: &OutPointHash) -> Vec<u8> {
        oph.as_inner().to_vec()
    }

    pub fn scripthash(&self) -> ScriptHash {
        ScriptHash::from_slice(&self.scripthash).unwrap()
    }
}

impl DBRow for OutToScripthashIndex {
    const CF: &'static str = OUTTOSCRIPTHASH_CF;
    fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self.outpointhash)
                .unwrap()
                .into_boxed_slice(),
            value: bincode::serialize(&self.scripthash)
                .unwrap()
                .into_boxed_slice(),
        }
    }

    fn from_row(row: &Row) -> Self {
        Self {
            outpointhash: bincode::deserialize(&row.key)
                .expect("failed to parse UnspentScriptHashIndexRow"),
            scripthash: bincode::deserialize(&row.value)
                .expect("failed to parse UnspentScriptHashIndexRow"),
        }
    }
}
