use crate::store::Row;

pub mod headerindex;
pub mod heightindex;
pub mod inputindex;
pub mod outputindex;
pub mod outputtokenindex;
pub mod outtoscriptindex;
pub mod scripthashindex;
pub mod tokenoutputindex;
pub mod unspentindex;

pub trait DBRow {
    const CF: &'static str;

    fn to_row(&self) -> Row;
    fn from_row(row: &Row) -> Self;
}
