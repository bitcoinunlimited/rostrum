use std::sync::{Mutex, MutexGuard};

use rayon::prelude::*;

use crate::{
    chaindef::OutPointHash,
    indexes::{
        headerindex::HeaderRow, heightindex::HeightIndexRow, inputindex::InputIndexRow,
        outputindex::OutputIndexRow, outputtokenindex::OutputTokenIndexRow,
        outtoscriptindex::OutToScripthashIndex, scripthashindex::ScriptHashIndexRow,
        tokenoutputindex::TokenOutputIndexRow, unspentindex::UnspentIndexRow, DBRow,
    },
    store::{Row, COLUMN_FAMILIES, META_CF},
};

pub struct UnspentEraseBatch {
    outpointhash: Mutex<Vec<OutPointHash>>,
}

impl Default for UnspentEraseBatch {
    fn default() -> Self {
        Self::new()
    }
}

impl UnspentEraseBatch {
    pub fn new() -> Self {
        Self {
            outpointhash: Default::default(),
        }
    }

    pub fn insert<I: IntoParallelIterator<Item = OutPointHash>>(&self, rows: I) {
        let mut outpoints: Vec<OutPointHash> = rows.into_par_iter().collect();
        self.outpointhash.lock().unwrap().append(&mut outpoints)
    }

    pub fn take_outpoints(&self) -> Vec<OutPointHash> {
        std::mem::take(&mut *self.outpointhash.lock().unwrap())
    }

    pub fn copy_outpoints(&self) -> Vec<OutPointHash> {
        self.outpointhash.lock().unwrap().clone()
    }
}

/**
 * For collecting multiple database entries and batch writing them in one transaction.
 */
pub struct WriteBatch {
    metadata: Mutex<Vec<Row>>,
    header: Mutex<Vec<Row>>,
    height: Mutex<Vec<Row>>,
    input: Mutex<Vec<Row>>,
    output: Mutex<Vec<Row>>,
    output_token: Mutex<Vec<Row>>,
    scripthash: Mutex<Vec<Row>>,
    token_output: Mutex<Vec<Row>>,
    unspent: Mutex<Vec<Row>>,
    outtoscripthash: Mutex<Vec<Row>>,
}

impl WriteBatch {
    pub fn new() -> Self {
        WriteBatch {
            metadata: Default::default(),
            header: Default::default(),
            height: Default::default(),
            input: Default::default(),
            output: Default::default(),
            output_token: Default::default(),
            scripthash: Default::default(),
            token_output: Default::default(),
            unspent: Default::default(),
            outtoscripthash: Default::default(),
        }
    }

    pub fn cf_to_vec_tuples(&self) -> Vec<(&'static str, &Mutex<Vec<Row>>)> {
        let map = vec![
            (META_CF, &self.metadata),
            (HeaderRow::CF, &self.header),
            (HeightIndexRow::CF, &self.height),
            (InputIndexRow::CF, &self.input),
            (OutputIndexRow::CF, &self.output),
            (OutputTokenIndexRow::CF, &self.output_token),
            (ScriptHashIndexRow::CF, &self.scripthash),
            (TokenOutputIndexRow::CF, &self.token_output),
            (UnspentIndexRow::CF, &self.unspent),
            (OutToScripthashIndex::CF, &self.outtoscripthash),
        ];
        assert!(map.len() == COLUMN_FAMILIES.len());
        map
    }

    pub fn insert<I: IntoParallelIterator<Item = Row>>(&self, cf_name: &str, rows: I) {
        let mut rows: Vec<Row> = rows.into_par_iter().collect();

        let mut locked = match cf_name {
            META_CF => self.metadata.lock().unwrap(),
            HeaderRow::CF => self.header.lock().unwrap(),
            HeightIndexRow::CF => self.height.lock().unwrap(),
            InputIndexRow::CF => self.input.lock().unwrap(),
            OutputIndexRow::CF => self.output.lock().unwrap(),
            OutputTokenIndexRow::CF => self.output_token.lock().unwrap(),
            ScriptHashIndexRow::CF => self.scripthash.lock().unwrap(),
            TokenOutputIndexRow::CF => self.token_output.lock().unwrap(),
            UnspentIndexRow::CF => self.unspent.lock().unwrap(),
            OutToScripthashIndex::CF => self.outtoscripthash.lock().unwrap(),
            _ => panic!("unknown cf family {cf_name}"),
        };
        locked.append(&mut rows);
    }

    /**
     * How many entries are in the current batch.
     */
    pub fn len(&self) -> usize {
        let locks: Vec<MutexGuard<Vec<Row>>> = self
            .cf_to_vec_tuples()
            .into_iter()
            .map(|(_, column)| column.lock().unwrap())
            .collect();
        locks.iter().map(|rows| rows.len()).sum()
    }
    /**
     * No entires in writebatch
     */
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

impl Default for WriteBatch {
    fn default() -> Self {
        Self::new()
    }
}
