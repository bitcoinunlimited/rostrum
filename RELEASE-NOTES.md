# Release notes

## 11.0.0 (2 Feb 2025)

This release has database changes that require the index
to be rebuilt. At first run after upgrade, rostrum will
trigger this reindex.

This release contains a new unspent output index, making
queries for unspent utxos and balance much more performant.

Many RPC calls have received the new filters, including
height filters that allow for historical queries, such
as "give me all utxos created after height A that existed
at height B".

A HTTP endpoint is added, allowing electrum queries via
HTTP POST requests (enable with --http).

- [bug] Improved P2P node connection handling (!360, !364)
- [build] Add musl target (!361) (by @vgrunner)
- [build] Make BCH a feature like Nexa (!365)
- [doc] Fix documentation after repo move (!375) (by @sickpig)
- [enhancement] Add HTTP endpoint (!378)
- [enhancement] Add unspent index (!371, !377)
- [enhancement] Allow TCP endpoint to be disabled (!383)
- [enhancement] Improved token metadata support for Nexa (!368, !373, !376) (by @ggriffith)
- [maintainance] Bump dependencies (!380, !366)
- [misc] Add chain defaults to python client (!356) (by @D10nysus)
- [performance] Batch scripthash subscription notifications (!381)
- [performance] Improve query time for token genesis (!359)
- [qa] Add merge request pipeline support (!374) (by @sickpig)
- [qa] QA fixes (!358, !382)
- [rpc] Add 'scriptpubkey' filter to mempool.get (!367)
- [rpc] Add height filters to RPC queries (!362)
- [rpc] Add token for BCH verbose transaction query (!357)
- [rpc] Limit and offset filters to RPC queries (!363)

## 10.1.0

This release contains numerous RPC improvements and some bug fixes.

* [bug] Add peer version to server.peers.subscribe (!353)
* [bug] Include token melts in token history (!351)
* [maintainance] Replace deprecated 'asyncore' (!352)
* [maintainance] Use latest released version of nexad, bchu anf bchn to execute rostrum functional tests (by @sickpig !347)
* [rpc] Add RPC call 'blockchain.block.header_verbose' (!345)
* [rpc] Add RPC call 'mempool.get' (!346)
* [rpc] Add broadcasted transactions to our mempool (!344)

Nexa specific:
* [bug] encoding/decoding on large p2st payloads (!354)
* [bug] Transactions fetched by idem missing height (!350)

BCH specific:
* [rpc] Add 'commitment' to token listunspent call (!343)


## 10.0.0

This release contains numerous RPC improvements and some bug fixes.

The websocket RPC interface is now rewritten to use the popular
[tungstenite](https://crates.io/crates/tokio-tungstenite) framework, which
fixed a lot of quirks and issues.

This release migrates the software to tokio async/await framework.

*Breaking change for BCH:*
The default behaviour for `token.transaction.get_history` is to now
also include transactions that mint NFTs. See documentation for details.

* [bug] Websocket fix for close event (by @vgrunner) (!337)
* [misc] Add a python websocket client (by @sickpig) (!330)
* [misc] Migrate codebase to tokio/async (!329 !328 !336, !324, !340)
* [misc] Recover if headers are missing in database (!332)
* [performance] Batch queries to rocksdb (!338)
* [rpc] Add addresses field to blockchain.utxo.get (!307)
* [rpc] Disconnect idle clients (!320)
* [rpc] Include subgroup data in token id filter (by @vgrunner) (!315)
* [rpc] Use leaky bucket in RPC timeout calculations (!322, !323)
* [rpc] Move to tungstenite-rs websocket (!325, !335)

Nexa specific:
* [bug] Fix address encoding with tokens (!304)
* [misc] Improve error messages when decoding p2st (!339)
* [rpc] Add addresses and group fields to inputs for blockchain.transaction.get (!307)
* [rpc] Allow txidem as parameter for blockchain.transaction.get (!310)
* [rpc] Fix filter by token and exclude authority amount (by @vgrunner) (!314)

BCH specific:
* [rpc] Add BCMR to token genesis for BCH (!312)
* [rpc] Add CRC20 to token genesis (!331, !333)
* [rpc] Change default behaviour for 'token.transaction.get\_history' (!318)

## 9.0.0 (6 Jul 2023)

This release contains a number of bug fixes and performance improvements.

This version requires a reindex. Rostrum will detect that it requires
a reindex and do so when upgraded.

* [bug] Decode p2sh32 correctly for *.address.* RPC calls (!282)
* [bug] Don't list unconfirmed spends in listunspent (!281)
* [bug] Fix db issue where version marker was not set (!290)
* [bug] Fix encoding/decoding issue for P2PKT (!284)
* [bug] Return last indexed blockchain tip, rather than last known (!298)
* [devops] Run qa tests with Windows build (via wine) (!293)
* [misc] Listen for exit signal in Windows (!293)
* [performance] Intant updates of new txs/blocks via P2P connection (!291)
* [performance] Remove use of slow RPC call getmempoolentry (!288)
* [qa] Enable token tests for BCHUnlimited
* [rpc] Add decimals to token genesis info (!287)
* [rpc] Add method 'mempool.count' (!295)
* [rpc] Add outpointhash to transaction.get (!286)
* [rpc] Add token filter to blockchain.scripthash/address (!283)
* [rpc] Don't accept RPC connections during initial index (!289)
* [rpc] Give error message to client on invalid json (!292)
* [rpc] Validate cursor argument in token.* calls (!299)

## 8.1.0 (3 May 2023)

This release adds additional output to existing RPC calls.

* [misc] Misc maintainance (!275, !276)
* [qa] Run functional tests on ARM64 (!241)
* [rpc] Add encoded group fields where missing (!272)
* [rpc] Add scriptpubkey & token details to utxo.get (!277)
* [rpc] Add tx fee, input amounds and token fields to blockchain.transaction.get (!269, !270, !271)

## 8.0.1 (19 January 2023)

Bug fix update.

Resolves database issues when upgrading from releases prior to Rostrum 8 (!267).
Corrects reverse outpointhash in the `blockchain.*.listunspent` call on Nexa (!266).

## 8.0.0 (17 January 2023)

This release includes enhancements and new features to improve
performance and connectivity.

Rostrum now includes peer discovery functionality that allows it to
connect to other Electrum servers, providing clients with a list
of online alternative servers through the `server.peers.subscribe` API call.

The database scheme has been updated to utilize the column family
feature of RocksDB for improved performance.

This release has database scheme changes and will trigger a reindex on first
startup after upgrade.

* [performance] Use RocksDB column family (!258)
* [performance] Use iterators when scanning db ranges (!252)
* [performance] Add connection pool to bitcoind/nexad (!256)
* [performance] Implement focused scripthash queries (!259)
* [rpc] Add peer discovery feature (!261)

## 7.0.0 (20 December 2022)

This release adds token support for Bitcoin Cash. This release implements
[CashTokens](https://github.com/bitjson/cashtokens).

Both Nexa and Bitcoin Cash get support for new RPC call `token.nft.list`.

This release has improved blockchain reorg handling. Rostrum will now cleanup
all index entries for blocks that have been orphaned.

This release has database scheme changes and will trigger a reindex on first
startup after upgrade.

* [bug] Send subscription notifications also for token transactions (!230)
* [bug] Unindex during reorg (!223)
* [maintainance] Bump rust to 1.65 (!228)
* [maintainance] CI updates (!231, !234, !245)
* [misc] Code cleanup (!225, !238, !243)
* [misc] Documentation updates (!226, !250)
* [qa] Move functional tests into this repo (!233, !236, !235)
* [qa] Update BCH support and add BCHN to functional tests (!237, !242, !246)
* [rpc] Add CashToken support (!244, !248, !249)
* [rpc] Add RPC `blockchain.address.decode` (!239)
* [rpc] Add RPC `blockchain.headers.tip` (!240)
* [rpc] Add RPC `token.nft.list` (!227)
* [rpc] Add `idem` and `tx_pos` fields to `blockchain.utxo.get` (!224)

## 6.0.0 (8 November 2022)

This release adds token support for the Nexa blockchain. Builds are now
available for both Win64 and GNU/ARM64. Builds for OS X work, but have
not yet been added to the CI.

This is the first token release. More token support and support for tokens
on Bitcoin Cash are planned for next release.

* [devops] Set a Minimum Supported Rust Version (msrv) (!205)
* [devops] Add GNU/ARM64 build to CI (!217)
* [devops] Add Win64 build to CI (!212)
* [devops] Misc CI improvements (!190, !219, !184, !191)
* [maintainance] Dependency changes & updates (!186, !187, !182, !203, !204, !216, !218)
* [maintainance] Remove unmaintained bulk indexer (!188)
* [maintainance] Implement ScriptHash as a type (!185)
* [misc] Introduce Chain for following the best chain (!195)
* [misc] Metrics updates from upstream (!192)
* [misc] Move header indexing into own module and document (!189)
* [performance] Add benchmark support (!209)
* [performance] Parallelize scripthash status history (!196)
* [rpc] Add RPC `token.address.get_balance` (!202)
* [rpc] Add RPC `token.address.get_history` (!202)
* [rpc] Add RPC `token.address.get_mempool` (!202)
* [rpc] Add RPC `token.address.listunspent` (!208)
* [rpc] Add RPC `token.genesis.info` (!210)
* [rpc] Add RPC `token.scripthash.get_balance` (!202)
* [rpc] Add RPC `token.scripthash.get_history` (!202)
* [rpc] Add RPC `token.scripthash.get_mempool` (!202)
* [rpc] Add RPC `token.scripthash.listunspent` (!208)
* [rpc] Add RPC `token.transaction.get_history` (!220)
* [token] Add more fields to transaction details (!200)
* [token] Add transaction token output index (!206)
* [token] Ignore tokens when calculating scripthash (!198)
* [token] Normalize scripthash in address conversion (!199)
* [token] nexa: Parse token from Script (!197)
* [token] rpc: Support cashaddr encoded token ID's (!211)
* [token] Add support for subgroup IDs (!207)

## 5.0.0 (31 August 2022)

This relase adds support for the Nexa blockchain. The database schema is
changed. The database now requires larger disk space (~200 GB for BCH),
but requires fewer calls to bitcoind.

This is a breaking change as database schema is different and cashaccount
indexing is disabled by default. Upgrading to this version will trigger a full
reindex.

* [doc] Document websockets (!169, !170 by @crystalclear7)
* [doc] Generate and publish documentation (!149)
* [doc] Generate configuration parameters docs (!149)
* [maintainance] Improve detection of default daemon dir (171)
* [maintainance] Misc. CI/CD improvements (!152, !157, !159, !168, 172, !173)
* [maintainance] Fix Dockerfile and add to CI/CD (!166)
* [misc] Add `--reindex` parameter (!177)
* [misc] Add `--version` parameter (!151)
* [misc] Add logo to the project (!144)
* [misc] Add support for Nexa blockchain (!154, !156, !158, !161, !162, !163, !167)
* [misc] Change schema to use output hashes (!155)
* [misc] Disable cashaccount indexing by default (!174)
* [misc] Document protocol methods (!164, !165)
* [performance] Enable compaction during IBD to avoid memory/disk spikes (!178)
* [performance] Use rayon to parallelize indexing. (!176)
* [rpc] Add RPC `server.donation_address` (!146)

## 4.0.0 (15 April 2022)

This version is a rebrand to Rostrum. This is a breaking change as file
path, configuration path, environment variables and prometheus names are
changed.

* [misc] Rebrand project to 'Rostrum'  (!143, !147)
* [maintainance] Replace GitHub CI with GitLab (!142)

## 3.1.0 (8 March 2022)

This release contains stability fixes, docker update and a new RPC call
`blockchain.utxo.get`.

The RPC call gives you data on a existing UTXO, or a UTXO that existed at
some point. If the output is spent, information about the spender is provided.
This allows a SPV client to call blockchain.transaction.get\_merkle to generate
a merkle branch, proving that it is spent.

*This project is moving to GitLab, as voted on in
[BUIP091](https://github.com/BitcoinUnlimited/BUIP/blob/master/091.md). More
info on this soon.*

* [bug] Error handling at client connect (#133)
* [bug] Fix Dockerfile build issues (by SuperCipher, #138)
* [bug] Ignore individual mempool transaction when fetch fails (#141)
* [maintainance] Clippy (linter) updates for newer Rust versions (#135, #139)
* [maintainance] Use tx outpoint struct from `bitcoincash` crate (#136)
* [misc] Avoid configuration footguns (#141)
* [misc] Rename `cookie` parameter to `auth` (#141)
* [rpc] Add RPC `blockchain.utxo.get` (#137)
* [rpc] Improved error message for invalid parameters (#137)

## 3.0.0 (21 January 2021)

### Relase notes

This version completes implementation of
[Protocol version 1.4.3](https://bitcoincash.network/electrum/). In addition
a custom call [`blockchain.transcation.get_confirmed_blockhash`](doc/rpc.md)
has been added to utilize the transaction index. This can replace bitcoind
txindex for those who want to optimize disk space.

WebSocket support has been added. As part of testing it, it has been used to
power the
[flipstarter overview site](https://flipstarters.bitcoincash.network/) and
[receipt generator](https://receipt.bitcoincash.network/) applications.

Support for Scalenet and Testnet4 has been added.

A bunch new metrics have been added, including [rpc
connections](https://bit.ly/3o5ADf4), [memory usage](https://bit.ly/2LI29m1)
and [cache churn](https://bit.ly/35ZGLiK) + more. See
[stats.bitcoincash.network](https://stats.bitcoincash.network) for demo with
all metrics.

The `blockchain.transcation.get` got a full overhaul and test coverage. It now
supports all fields as bitcoind. It is **more** consistent than bitcoind, as
every implementation (BU, BCHN, ...) has its quirks and in addition remove/add
fields depending on various transaction properties, such as if it's coinbase,
unconfirmed etc. ElectrsCash never removes or adds a field.

This is a major version release, which implies breaking changes. Please see
below.

### Changes

* [bug] Fix compatibility with Bitcoin ABC (#93)
* [bug] Fix issue "fee must be in decreasing order" (#98)
* [dos] Add RPC connection limit (#113)
* [dos] Limit connections by IP prefix (#114)
* [dos] Add scripthash subscription limit (#107)
* [maintainance] Bump crate dependencies and replace abandoned crates (#118)
* [maintainance] Remove batch fetching of unconfirmed transactions (#117)
* [maintainance] Remove use of `mut self` in rpc blockchain (#122)
* [maintainance] Replace use of `bitcoin_hashes` crate with `bitcoincash` (#97)
* [maintainance] Split out responsibility of Query (#116)
* [maintainance] Use prometheus imports (#100)
* [metrics] Add metric `electrscash_process_jemalloc_allocated` (#104)
* [metrics] Add metric `electrscash_process_jemalloc_resident` (#104)
* [metrics] Add metric `electrscash_rockdb_mem_readers_total` (#103)
* [metrics] Add metric `electrscash_rockdb_mem_table_total` (#103)
* [metrics] Add metric `electrscash_rockdb_mem_table_unflushed` (#103)
* [metrics] Add metric `electrscash_rpc_connections_rejeced_global` (#125)
* [metrics] Add metric `electrscash_rpc_connections_rejeced_prefix` (#125)
* [metrics] Add metric `electrscash_rpc_connections_total` (#125)
* [metrics] Add metric `electrscash_rpc_connections` (#125)
* [misc] Add WebSocket support (#109)
* [misc] Add testnet4 and scalenet support (#130)
* [misc] Remove --txid-limit (#106)
* [misc] Support more argument datatypes in python cli (#95)
* [misc] Update confiration defaults (#124)
* [misc] Use random eviction cache (#101)
* [performance] Change semantics of index-batch-size (#121)
* [performance] Store statushashes as FullHash (#105)
* [performance] Use generators with `load_txns_by_prefix` (#126)
* [performance] Use parallel iter for finding inputs/outputs (#127)
* [rpc] Add RPC `blockchain.address.get_mempool` (#120)
* [rpc] Add RPC `blockchain.address.subscribe` and `blockchain.address.unsubscribe`. (#108)
* [rpc] Add RPC `blockchain.scripthash.get_mempool` (#120)
* [rpc] Add RPC `blockchain.transaction.get_confirmed_blockhash` (#96)
* [rpc] Improve likeness with bitcoind getrawtransaction (#119, #128, #129)
* [rpc] Make height optional in `transaction.get_merkle` (#111)

### Breaking changes

#### Default listening interface

The default listening interfaces for the RPC has been changed from localhost to
all interfaces. The WebSocket interface also listens on all interfaces.

This projects goal is to be a high performant public Electrum server. Unlike
this projects predecessor, which aims to be a private server on your local
machine.

#### Parameter `--txid-limit` removed

The `--txid-limit` DoS parameter is removed. Please use `--rpc-timeout`
parameter instead for more accurate DoS limit.

For backward compatibility with existing configurations, this argument still
exists but now does nothing. It will be completely removed in next major
version of ElectrsCash.

#### Changes in `blockchain.transaction.get` verbose output

**tldr;** The `value` field in the `vout` entries of verbose transaction output is
replaced with `value_satoshis` and `value_coins`. It used to be in satoshis.

The `value` field will be reintroduced in later version of ElectrsCash
in unit coins rather than satoshis (satoshis / 100 000 000).

**Details:** The verbose output of `blockchain.transaction.get` is implemented
in ElectrsCash, rather than forwarded to the bitcoind node as stated in the
specification. This is a massive performance increase, as the transaction is
often in the local cache.

It was discovered that the `value` of `vout` entries were in satoshis, compared
to bitcoind where it is in coins (satoshis / 100 000 000). While satoshis is
more consistent with the rest of the electrum specification, it is incorrect
as the specification for this RPC call is to output "whatever bitcoind outputs".

Rather than simply changing the unit of the field at the risk of software that
we don't know of using this field silently failing, it has been temporarily
removed to allow such software to properly fail and corrected. The `value`
field will be re-added in a later version of ElectrsCash.

## 2.0.0 (21 August 2020)
* [bug] Fix deadlock on Ctrl+C (#89)
* [bug] Update subscription statistics correctly (#89)
* [contrib] Add generic client for testing RPC methods (#83)
* [misc] Add a 'blocks-dir' option analogous to bitcoind's '-blocksdir' (#89)
* [misc] Add configuration `wait-duration-secs` to set custom main loop wait duration
* [misc] External notification of block update via SIGUSR1 (#78)
* [misc] Improve rpc timeout trigger (#81)
* [misc] Log progress while waiting for IBD (#76)
* [misc] Switch from `bitcoin` crate dependency to `bitcoincash` (#88)
* [rpc] Add `blockchain.address.get_balance` (#72)
* [rpc] Add `blockchain.address.get_first_use`
* [rpc] Add `blockchain.address.get_first_use` (#72)
* [rpc] Add `blockchain.address.get_history` (#72)
* [rpc] Add `blockchain.address.get_scripthash` (#85)
* [rpc] Add `blockchain.address.listunspent` (#72)
* [rpc] Add `blockchain.scripthash.unsubscribe` (#71)
* [rpc] Show fee for unconfirmed transactions (#89)
* [rpc] Update `blockchain.scripthash.get_first_use`

### Breaking changes

This release contains an RPC optimization that requires Bitcoin Unlimited v1.9
or BCHN v22.0.0 and will not work with older versions.

## 1.1.1 (10 April 2020)
* [bug] Fix protocol-negotiation in `server.version` response (#61)
* [bug] Fix dropped notification due to full client buffer (#60)
* [bug] Reduce log level on client errors (#64)

This is a bug-fix only release. No breaking changes.

## 1.1.0 (6 April 2020)
* [bug] Don't index cashaccounts before activation height
* [misc] Add database version and reindex on incompatible database.
* [misc] Allow loading config file from specified place via `--conf`
* [misc] Allow setting `--cookie-file` path via configuration
* [misc] Clean up RPC threads after connection is closed
* [misc] Setting `--network` now takes `bitcoin` instead of `mainnet` as parameter
* [performance] Better Cashaccount memory handling.
* [performance] Better client subscription change detection
* [performance] Better db indexes for faster scripthash lookups.
* [performance] Better transaction caching.
* [qa] Script for running integration tests.
* [rpc] Add RPC timeouts (DoS mitigation)
* [rpc] Bump protocol version to 1.4.1
* [rpc] Identify transactions with unconfirmed parents
* [rpc] Implement `blockchain.scripthash.get_first_use` RPC method.
* [rpc] Implement `server.features` RPC method.
* [rpc] Improved error messages with error codes.
* [rpc] Return error on unknown method rather than disconnecting the client.
* [rpc] Use bitcoind's relay fee rather than hardcoded.

Note: This release has database changes incompatible with previous versions.
At first startup after the upgrade, ElectrsCash will do a full reindex.

## 1.0.0 (18 September 2019)

* Cache capacity is now defined in megabytes, rather than number of entries.
* Support Rust >=1.34
* Use `configure_me` instead of `clap` to support config files, environment variables and man pages (@Kixunil)
* Revert LTO build (to fix deterministic build)
* Allow stopping bulk indexing via SIGINT/SIGTERM
* Cache list of transaction IDs for blocks
* Prefix Prometheus metrics with 'electrscash_'

## 0.7.0 (13 June 2019)

* Support Bitcoin Core 0.18
* Build with LTO
* Allow building with latest Rust (via feature flag)
* Use iterators instead of returning vectors (@Kixunil)
* Use atomics instead of `Mutex<u64>` (@Kixunil)
* Better handling invalid blocks (@azuchi)

## 0.6.2 (17 May 2019)

* Support Rust 1.32 (for Debian)

## 0.6.1 (9 May 2019)

* Fix crash during initial sync
* Switch to `signal-hook` crate

## 0.6.0 (29 Apr 2019)

* Update to Rust 1.34
* Prefix Prometheus metrics with 'electrs_'
* Update RocksDB crate to 0.12.1
* Update Bitcoin crate to 0.18
* Support latest bitcoind mempool entry vsize field name
* Fix "chain-trimming" reorgs
* Serve by default on IPv4 localhost

## 0.5.0 (3 Mar 2019)

* Limit query results, to prevent RPC server to get stuck (see `--txid-limit` flag)
* Update RocksDB crate to 0.11
* Update Bitcoin crate to 0.17

## 0.4.3 (23 Dec 2018)

* Support Rust 2018 edition (1.31)
* Upgrade to Electrum protocol 1.4 (from 1.2)
* Let server banner be configurable via command-line flag
* Improve query.get_merkle_proof() performance

## 0.4.2 (22 Nov 2018)

* Update to rust-bitcoin 0.15.1
* Use bounded LRU cache for transaction retrieval
* Support 'server.ping' and partially 'blockchain.block.header' Electrum RPC

## 0.4.1 (14 Oct 2018)

* Don't run full compaction after initial import is over (when using JSONRPC)

## 0.4.0 (22 Sep 2018)

* Optimize for low-memory systems by using different RocksDB settings
* Rename `--skip_bulk_import` flag to `--jsonrpc-import`

## 0.3.2 (14 Sep 2018)

* Optimize block headers processing during startup
* Handle TCP disconnections during long RPCs
* Use # of CPUs for bulk indexing threads
* Update rust-bitcoin to 0.14
* Optimize block headers processing during startup


## 0.3.1 (20 Aug 2018)

* Reconnect to bitcoind only on transient errors
* Poll mempool after transaction broadcasting

## 0.3.0 (14 Aug 2018)

* Optimize for low-memory systems
* Improve compaction performance
* Handle disconnections from bitcoind by retrying
* Make `blk*.dat` ingestion more robust
* Support regtest network
* Support more Electrum RPC methods
* Export more Prometheus metrics (CPU, RAM, file descriptors)
* Add `scripts/run.sh` for building and running `electrs`
* Add some Python tools (as API usage examples)
* Change default Prometheus monitoring ports

## 0.2.0 (14 Jul 2018)

* Allow specifying custom bitcoind data directory
* Allow specifying JSONRPC cookie from commandline
* Improve initial bulk indexing performance
* Support 32-bit systems

## 0.1.0 (2 Jul 2018)

* Announcement: https://lists.linuxfoundation.org/pipermail/bitcoin-dev/2018-July/016190.html
* Published to https://crates.io/electrs and https://docs.rs/electrs
