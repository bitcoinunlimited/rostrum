extern crate configure_me_codegen;

fn main() -> Result<(), configure_me_codegen::Error> {
    println!("cargo::rustc-check-cfg=cfg(bch)");
    println!("cargo::rustc-check-cfg=cfg(nexa)");

    let is_bch_enabled = std::env::var("CARGO_FEATURE_BCH").is_ok();
    let is_nexa_enabled = std::env::var("CARGO_FEATURE_NEXA").is_ok();

    if is_bch_enabled && is_nexa_enabled {
        panic!("Features 'nexa' and 'bch' are mutually exclusive. Enable only one of them.");
    }

    // default to nexa
    let is_nexa_enabled = if !is_bch_enabled && !is_nexa_enabled {
        true
    } else {
        is_nexa_enabled
    };

    if is_bch_enabled {
        println!("cargo:rustc-cfg=bch");
    }

    if is_nexa_enabled {
        println!("cargo:rustc-cfg=nexa");
    }

    configure_me_codegen::build_script_auto()
}
