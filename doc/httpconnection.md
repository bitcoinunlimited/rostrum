# HTTP Connection

By enabling the `--http` switch on rostrum, you let rostrum accept requests via plain HTTP connections. See [configuration document](configuration.md) on how to enable this switch. See also [default ports](ports.md) for how to connect.

Requests are submitted as `POST` request, with the same request format as with other protocols as the body content.

## wget example

> wget --post-data='{"method": "server.ping", "params": []}' -q http://localhost:31400 -O -
>
> {"id":0,"jsonrpc":"2.0","result":null}


## Python example

```
import http.client
import json

conn = http.client.HTTPConnection("localhost", 31400)
data = {"method": "server.ping", "params": []}
conn.request("POST", "/", body=json.dumps(data))

response = conn.getresponse()
print(json.loads(response.read().decode()))
```

# Node example

```
const http = require('http');

const data = JSON.stringify({ method: "server.ping", params: [] });

const options = {
  hostname: 'localhost',
  port: 31400,
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
};

http.request(options, (res) => {
  res.setEncoding('utf8');
  res.on('data', console.log);
}).on('error', console.error)
  .end(data);
```