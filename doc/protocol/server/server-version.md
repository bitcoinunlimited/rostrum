## [server.version](site:/protocol/server/server-version)

Identify the client to the server and negotiate the protocol version.
Only the first `server.version`{.interpreted-text role="func"} message
is accepted.

**Signature**

> Function:
> server.version(client\_name=\"\", protocol\_version=\"1.4\")
>
> -   *client_name*
>
>     A string identifying the connecting client software.
>
> -   *protocol_version*
>
>     An array `[protocol_min, protocol_max]`, each of which is a
>     string. If `protocol_min` and `protocol_max` are the same, they
>     can be passed as a single string rather than as an array of two
>     strings, as for the default value.
>
> The server should use the highest protocol version both support:
>
>     version = min(client.protocol_max, server.protocol_max)
>
> If this is below the value:
>
>     max(client.protocol_min, server.protocol_min)
>
> then there is no protocol version in common and the server must close
> the connection. Otherwise it should send a response appropriate for
> that protocol version.

**Result**

> An array of 2 strings:
>
> > `[server_software_version, protocol_version]`
>
> identifying the server and the protocol version that will be used for
> future communication.

**Example**:

    server.version("Electron Cash 3.3.6", ["1.2", "1.4"])

**Example Result**:

    ["Rostrum 2.2.3", "1.4"]