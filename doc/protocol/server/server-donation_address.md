## [server.donation\_address](site:/protocol/server/server-donation_address)

Return a server donation address.

**Signature**

> Function:
> server.donation\_address()

**Result**

> A string.

**Example Result**

>     "1BWwXJH3q6PRsizBkSGm2Uw4Sz1urZ5sCj"