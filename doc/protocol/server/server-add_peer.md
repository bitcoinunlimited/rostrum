## [server.add\_peer](site:/protocol/server/server-add_peer)

A newly-started server uses this call to get itself into other servers'
peers lists. It sould not be used by wallet clients.

**Signature**

> Function:
> server.add\_peer(features)
>
> Version added:
> 1.1
>
> -   *features*
>
>     The same information that a call to the sender\'s
>     `server.features` RPC call would
>     return.

**Result**

> A boolean indicating whether the request was tentatively accepted. The
> requesting server will appear in `server.peers.subscribe` when further
> sanity checks complete successfully.