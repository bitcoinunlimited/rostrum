## [server.ping](site:/protocol/server/server-ping)

Ping the server to ensure it is responding, and to keep the session
alive. The server may disconnect clients that have sent no requests for
roughly 10 minutes.

**Signature**

> Function:
> server.ping()
> :::
>
> Version added:
> 1.2
> :::

**Result**

> Returns `null`.