## [server.peers.subscribe](site:/protocol/server/server-peers-subscribe)

Return a list of peer servers. Despite the name this is not a
subscription and the server must send no notifications.

**Signature**

> Function:
> server.peers.subscribe()

**Result**

> An array of peer servers, each returned as a 3-element array. For
> example:
>
>     ["107.150.45.210",
>      "e.anonyhost.org",
>      ["v1.0", "p10000", "t", "s995"]]
>
> The first element is the IP address, the second is the host name
> (which might also be an IP address), and the third is a list of server
> features. Each feature and starts with a letter. \'v\' indicates the
> server maximum protocol version, \'p\' its pruning limit and is
> omitted if it does not prune, \'t\' is the TCP port number, and \'s\'
> is the SSL port number. If a port is not given for \'s\' or \'t\' the
> default port for the coin network is implied. If \'s\' or \'t\' is
> missing then the server does not support that transport.