## [server.features](site:/protocol/server/server-features)

Return a list of features and services supported by the server.

**Signature**

> Function:
> server.features()
>
> Version changed:
> 1.4.2 *hosts* key is no longer required, but recommended.

**Result**

> A dictionary of keys and values. Each key represents a feature or
> service of the server, and the value gives additional information.
>
> The following features MUST be reported by the server. Additional
> key-value pairs may be returned.
>
> -   *genesis_hash*
>
>     The hash of the genesis block. This is used to detect if a peer is
>     connected to one serving a different network.
>
> -   *hash_function*
>
>     The hash function the server uses for `script hashing`. The client must
>     use this function to hash pay-to-scripts to produce script hashes
>     to send to the server. The default is "sha256". "sha256" is
>     currently the only acceptable value.
>
> -   *server_version*
>
>     A string that identifies the server software. Should be the same
>     as the result to the `server.version` RPC call.
>
> -   *protocol_max*
>
> -   *protocol_min*
>
>     Strings that are the minimum and maximum Electrum protocol
>     versions this server speaks. Example: "1.1".
>
> -   *pruning*
>
>     An integer, the pruning limit. Omit or set to
>     `null` if there is no pruning
>     limit. Should be the same as what would suffix the letter `p` in
>     the IRC real name.
>
> The following features are RECOMMENDED that be reported by the
> servers.
>
> -   *hosts*
>
>     A dictionary, keyed by host name, that this server can be reached
>     at. If this dictionary is missing, then this is a way to signal to
>     other servers that while this host is reachable, it does not wish
>     to peer with other servers. A server SHOULD stop peering with a
>     peer if it sees the *hosts* dictionary for its peer is empty
>     and/or no longer contains the expected route (e.g. hostname).
>     Normally this dictionary will only contain a single entry; other
>     entries can be used in case there are other connection routes
>     (e.g. Tor).
>
>     The value for a host is itself a dictionary, with the following
>     optional keys:
>
>     -   *ssl_port*
>
>         An integer. Omit or set to `null`
>          if SSL connectivity is not provided.
>
>     -   *tcp_port*
>
>         An integer. Omit or set to `null`
>         if TCP connectivity is not provided.
>
>     -   *ws_port*
>
>         An integer. Omit or set to `null`
>         if Web Socket (<ws://>) connectivity is not
>         provided.
>
>     -   *wss_port*
>
>         An integer. Omit or set to `null`
>         if Web Socket Secure (<wss://>) connectivity is
>         not provided.
>
>     A server should ignore information provided about any host other
>     than the one it connected to.

**Example Result**

    {
        "genesis_hash": "000000000933ea01ad0ee984209779baaec3ced90fa3f408719526f8d77f4943",
        "hosts": {"14.3.140.101": {"tcp_port": 51001, "ssl_port": 51002}},
        "protocol_max": "1.0",
        "protocol_min": "1.0",
        "pruning": null,
        "server_version": "ElectrumX 1.0.17",
        "hash_function": "sha256"
    }