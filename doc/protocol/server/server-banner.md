## [server.banner](site:/protocol/server/server-banner)

Return a banner to be shown in the Electrum console.

**Signature**

> Function:
> server.banner()

**Result**

> A string.

**Example Result**

>     "Welcome to Rostrum!"