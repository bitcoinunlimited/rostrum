## [mempool.get](site:/protocol/mempool/mempool-get)

Fetch a list of transactions in the mempool

**Signature**

> Function:
> blockchain.mempool.get(filter (optional))
>
> -   *filter*
>
>     Filter what transactions are returned. Filter is a dictionary with the following options as keys:
>
>     - scriptsig - Value hex string. Include transactions that partially matches scriptsig in inputs.
>     - scriptpubkey - Value in hex. Include transaction that partially matches scriptpubkey in outputs.
>     - operation - Set to 'union' to include results matching any filter; 'except' to include results matching all (default: 'except').
>
> Version added:
> Rostrum 10.1
>
> Version history:
> Rostrum 11: Added 'scriptpubkey' and 'operation'

**Result**

> Dictionary with transaction list

 **Example Results**

    {
        "transactions":  ["6ba738f581968c915a097015fc32d4c951a222d8209a21b0163399bf57622027", "dbbe9a005b0f53eea5b63e3a4f5a36758cd736259d8036c39a4e5a3d7838b1a5"]
    }

