## [mempool.count](site:/protocol/mempool/mempool-count)

Return the number of transactions in the mempool.

**Signature**

> Function:
> blockchain.mempool.count()
>
> Version added:
> Rostrum 9.0
>

**Result**

> Dictionary with mempool count

 **Example Results**

    {
        "count": 546
    }
