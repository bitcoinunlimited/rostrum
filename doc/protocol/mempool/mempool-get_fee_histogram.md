## [mempool.get\_fee\_histogram](site:/protocol/mempool/mempool-get_fee_histogram)

Return a histogram of the fee rates paid by transactions in the memory
pool, weighted by transaction size.

**Signature**

> Function:
> mempool.get\_fee\_histogram()
>
> Version added:
> 1.2

**Result**

> The histogram is an array of \[*fee*, *vsize*\] pairs, where is the
> cumulative virtual size of mempool transactions with a fee rate in the
> interval \[, \], and \> .
>
> Fee intervals may have variable size. The choice of appropriate
> intervals is currently not part of the protocol.

**Example Result**

>     [[12, 128812], [4, 92524], [2, 6478638], [1, 22890421]]