## [token.genesis.info](site:/protocol/token/token-genesis-info)

Info from token creation transaction.

=== "Bitcoin Cash"
    **Signature**

    > Function:
    > token.genesis.info(token)
    >
    > Version added:
    > Rostrum 6.0
    >
    > -   *token*
    >
    >     Token ID as a hexadecimal string.

    **Result Example**

            {
                "tx_hash": "503a3af8363d506c18d99efe538122947911038b5b4f8a0d62d6127e548bb13c",
                "height": 102,
                "bcmr": {
                    "hash": "cef3a7be7f95f3c27f393c74c99a2c1f1f8c658d3cd973c5ff3c266c7133aa38",
                    "uris": [
                        "https://example.org/bcmr.json"
                    ]
                },
                "crc20": {
                    "symbol": "SYMBOL",
                    "name": "Token Name"
                    "decimals": 8,
                    "genesis_out": "f8a0d62d6127e548bb13c503a3af8363d506c18d99efe538122947911038b5b4"
                }
            }


=== "Nexa"
    **Signature**

    > Function:
    > token.genesis.info(token)
    >
    > Version added:
    > Rostrum 6.0
    >
    > -   *token*
    >
    >     Token ID as cashaddr encoded or hexadecimal string.

    **Result**

    > Dictionary of token details.
    >
    > Note: The `decimal_places` can be null, this happens if token description has encoded a value
    > for decimal places, but the encoded value is invalid (not a number, or a number larger than 18 decimal places).
    > If `decimal_places` is not encoded into the token description, this implies 0.
    >
    > Note: Genesis must be confirmed for rostrum to detect it.

    **Result Example**

            {
                "document_hash": "cef3a7be7f95f3c27f393c74c99a2c1f1f8c658d3cd973c5ff3c266c7133aa38",
                "document_url": "https://example.org",
                "decimal_places": 10,
                "height": 102,
                "name": "TEST",
                "ticker": "TEST",
                "group: "nexareg:tzfl3s4qp4hzvjf2t6cy2eeyc67896pyflhlx6na382t2sapmyqqq3z44qjuc",
                "token_id_hex": 93f8c2a00d6e26492a5eb0456724c6bc72e8244feff36a7d89d4b543a1d90000,
                "txid": "503a3af8363d506c18d99efe538122947911038b5b4f8a0d62d6127e548bb13c",
                "txidem": "316862b66bf34ab6a7c24f6714e91ced836b50b540a5f40cf6f94499f14d322d"
                "op_return": ""6a0438564c05065449434b455209536f6d65204e616d651368747470733a2f2f6578616d706c652e6f726720ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff5b
            }
