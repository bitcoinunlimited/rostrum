## [token.scripthash.get\_history](site:/protocol/token/token-scripthash-get_history)

Return the confirmed and unconfirmed token history of a `script hash`

**Signature**

> Function:
> token.scripthash.get\_history(scripthash, cursor *(optional)*, token *(optional)*, { "from_height": 0, ...} *(optional)*)
>
> Version added:
> Rostrum 6.0.
>
--8<-- "doc/protocol/param_scripthash.ext"
--8<-- "doc/protocol/param_cursor.ext"
--8<-- "doc/protocol/param_token.ext"
--8<-- "doc/protocol/param_filter_height.ext"

**Result**

> A dictionary with a cursor and a list of confirmed transactions in blockchain order, including mempool.
> Each confirmed transaction is an array with
> the following keys:
>
> -   *height*
>
>     The integer height of the block the transaction was confirmed in.
>
> -   *tx_hash*
>
>     The transaction hash in hexadecimal.
>
> See `token.scripthash.get_mempool`
> for how mempool transactions are returned.
>
> If the cursor is non-null, this denotes that the history retuned is incomplete and suggests the client call the same query again with cursor provided
> for continuation of history. If cursor is null, it denotes the latest history entry is provided in current response.

**Result Examples**

    {
      "transactions": [
      {
        "height": 200004,
        "tx_hash": "acc3758bd2a26f869fcc67d48ff30b96464d476bca82c1cd6656e7d506816412"
      },
      {
        "height": 215008,
        "tx_hash": "f3e1bf48975b8d6060a9de8884296abb80be618dc00ae3cb2f6cee3085e09403"
      {,
      {
        "fee": 20000,
        "height": 0,
        "tx_hash": "9fbed79a1e970343fcd39f4a2d830a6bde6de0754ed2da70f489d0303ed558ec"
      }],
      "cursor": "t1S7vY94La"
    }