## [token.address.get\_history](site:/protocol/token/token-address-get_history)

Return the confirmed and unconfirmed token history of a Nexa or Bitcoin Cash address.

**Signature**

> Function:
> token.address.get\_history(address, cursor *(optional)*, token *(optional)*, filter { "from_height": 0, ...} *(optional)*)
>
> Version added:
> Rostrum 6.0
>
--8<-- "doc/protocol/param_address.ext"
--8<-- "doc/protocol/param_cursor.ext"
--8<-- "doc/protocol/param_token.ext"
--8<-- "doc/protocol/param_filter_height.ext"


**Result**

> See [token.scripthash.get_history](site:/protocol/token/token-scripthash-get_history)