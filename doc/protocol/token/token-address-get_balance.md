## [token.address.get\_balance](site:/protocol/token/token-address-get_balance)

Return the confirmed and unconfirmed balances of tokens in a Bitcoin Cash or Nexa address.

**Signature**

> Function:
> token.address.get\_balance(address, cursor *(optional)*, token *(optional)*)
>
> Version added: Rostrum 6.0
>
--8<-- "doc/protocol/param_address.ext"
--8<-- "doc/protocol/param_cursor.ext"
--8<-- "doc/protocol/param_token.ext"

**Result**

> See [token.scripthash.get_balance](site:/protocol/token/token-scripthash-get_balance).