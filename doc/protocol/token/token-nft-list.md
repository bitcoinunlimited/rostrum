## [token.nft.list](site:/protocol/token/token-nft-list)

Return list of all NFT's minted from a specified parent token.

=== "Bitcoin Cash"
    **Signature**

    > Function:
    > token.nft.list(token, cursor *(optional)*)
    >
    > Version added:
    > Rostrum 7.0
    >
    --8<-- "doc/protocol/param_token.ext"
    --8<-- "doc/protocol/param_cursor.ext"

    **Result**

    > A list of all NFT's minted from given token ID.
    >
    > - *nft*
    >
    >   List of NFT dictionaries with the following keys:
    >
    > > -   *token_id*
    > >
    > >    The token identifier as a hexadecimal string
    > >
    > > -   *commitment*
    > >
    > >     The NFT commitment as a hexadecimal string
    >
    > -   *cursor*
    >
    >     Null if it was the last (or the full) result set. If there are more results to
    >     fetch, call the method again with this cursor value to get next set of results.

    **Result Examples**

        {
        "nft": [
            {
            "token_id": "9fbed79a1e970343fcd39f4a2d830a6bde6de0754ed2da70f489d0303ed558ec6bca82c1",
            "commitment": "c0754a"
            },
        ],
        "cursor": null
        }

=== "Nexa"
    **Signature**

    > Function:
    > token.nft.list(token, cursor *(optional)*)
    >
    > Version added:
    > Rostrum 7.0
    >
    --8<-- "doc/protocol/param_token.ext"
    --8<-- "doc/protocol/param_cursor.ext"

    **Result**

    > A list of all NFT's minted from parent ID.
    >
    > - *nft*
    >
    >   List of NFT dictionaries with the following keys:
    >
    > > -   *group*
    > >
    > >    The token identifier as cashaddr
    > >
    >
    > > -   *token_id_hex*
    > >
    > >    The token identifier as a hexadecimal string
    > >
    >
    > -   *cursor*
    >
    >     Null if it was the last (or the full) result set. If there are more results to
    >     fetch, call the method again with this cursor value to get next set of results.

    **Result Examples**

        {
        "nft": [
            {
            "group": "nexareg:tzfl3s4qp4hzvjf2t6cy2eeyc67896pyflhlx6na382t2sapmyqqq3z44qjuc",
            "token_id_hex": "9fbed79a1e970343fcd39f4a2d830a6bde6de0754ed2da70f489d0303ed558ec6bca82c1"
            },
        ],
        "cursor": null
        }
