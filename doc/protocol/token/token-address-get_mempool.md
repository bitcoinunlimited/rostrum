## [token.address.get\_mempool](site:/protocol/token/token-address-get_mempool)

Return the unconfirmed token transactions of a Nexa or Bitcoin Cash address.

**Signature**

> Function:
> token.address.get\_mempool(address, cursor *(optional)*, token *(optional)*, filter { "from_height": 0, ...} *(optional)*)
>
> Version added:
> Rostrum 6.0
>
--8<-- "doc/protocol/param_address.ext"
--8<-- "doc/protocol/param_cursor.ext"
--8<-- "doc/protocol/param_token.ext"
--8<-- "doc/protocol/param_filter_offsetlimit.ext"

**Result**

> As for [token.scripthash.get_mempool](site:/protocol/token/token-scripthash-get_mempool)