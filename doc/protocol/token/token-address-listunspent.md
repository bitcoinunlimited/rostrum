## [token.address.listunspent](site:/protocol/token/token-address-listunspent)

Return an list of token UTXOs sent to a Nexa or Bitcoin Cash address.

**Signature**

> Function:
> token.address.listunspent(address, cursor *(optional)*, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
--8<-- "doc/protocol/param_address.ext"
--8<-- "doc/protocol/param_cursor.ext"
--8<-- "doc/protocol/param_token.ext"

**Result**

> As for [token.scripthash.listunspent](site:/protocol/token/token-scripthash-listunspent)