## [token.scripthash.get\_balance](site:/protocol/token/token-scripthash-get_balance)

Return the confirmed and unconfirmed balances of tokens in a `script hash`. Token is optional,
if provided, will filter on token.

**Signature**

> Function:
> token.scripthash.get\_balance(scripthash, cursor *(optional)*, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -    *scripthash*
>
>      The script hash as a hexadecimal string.
>
> -   *cursor*
>
>     If the address has a large amount of tokens, the results may be paginated. The cursor is `null` if this result was the
>     last (or the full) result set. If there are more results to
>     fetch, call the method again with this cursor value to get next set of results.
>
> -   *token (optional)*
>
>     Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.


**Result**

> Dictionary of keys `confirmed` and `unconfirmed`. The value of each is dict of tokens and
> the amount in given scripthash.

**Result Example**

    {
      "confirmed": {
        "3c469e9d6c5875d37a43f353d4f88e61fcf812c66eee3457465a40b0da4153e0": 103873966
      },
      "unconfirmed": {
        "3c469e9d6c5875d37a43f353d4f88e61fcf812c66eee3457465a40b0da4153e0": 236844
      },
      "cursor": "t1S7vY94La"
    }
