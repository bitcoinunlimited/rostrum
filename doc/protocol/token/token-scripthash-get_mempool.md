## [token.scripthash.get\_mempool](site:/protocol/token/token-scripthash-get_mempool)

Return the unconfirmed token transactions of a `script hash`.

**Signature**

> Function:
> token.scripthash.get\_mempool(scripthash, cursor *(optional)*, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -   *scripthash*
>
>     The script hash as a hexadecimal string.
>
> -   *cursor (optional)*
>
>     Null or string. If null, indicates that all histoy is requested. If a cursor
>     is provided from an earlier call, history is continued from last transaction in earlier call.
>
> -   *token (optional)*
>
> -   Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.

**Result**

> A dictionary with a transaction list of mempool transactions
> in arbitrary order. Each mempool transaction is a dictionary
> with the following keys:
>
> -   *height*
>
>     `0` if all inputs are confirmed, and `-1` otherwise.
>
> -   *tx_hash*
>
>     The transaction hash in hexadecimal.
>
> -   *fee*
>
>     The transaction fee in minimum coin units (satoshis).
>
> -   *cursor*
>
>     Null if it was the last (or the full) result set. If there are more results to
>     fetch, call the method again with this cursor value to get next set of results.

**Result Example**

    {
        "transactions": [
        {
            "tx_hash": "45381031132c57b2ff1cbe8d8d3920cf9ed25efd9a0beb764bdb2f24c7d1c7e3",
            "height": 0,
            "fee": 24310
        }
        ],
        "cursor": "t1S7vY94La"
    ]