## [blockchain.transaction.broadcast](site:/protocol/blockchain/blockchain-transaction-broadcast)

Broadcast a transaction to the network.

**Signature**

> Function:
> blockchain.transaction.broadcast(raw\_tx)
>
> *raw_tx*
>
> > The raw transaction as a hexadecimal string.

**Result**

> On Bitcoin Cash the transaction hash (aka txid) as a hexadecimal string.
> On Nexa the transaction idem (txidem) as a hexadecimal string.
>
**Result Examples**

    "a76242fce5753b4212f903ff33ac6fe66f2780f34bdb4b33b175a7815a11a98e"

