## [blockchain.block.headers](site:/protocol/blockchain/blockchain-block-headers)

Return a concatenated chunk of block headers from the main chain.

**Signature**

> Function:
> blockchain.block.headers(start\_height, count, cp\_height=0)
>
> Version added:
> 1.2
>
> Version changed:
> 1.4 *cp_height* parameter added
>
> Version changed:
> 1.4.1
>
> *start_height*
>
> > The height of the first header requested, a non-negative integer.
>
> *count*
>
> > The number of headers requested, a non-negative integer.
>
> *cp_height*
>
> > Checkpoint height, a non-negative integer. Ignored if zero,
> > otherwise the following must hold:
> >
> > > *start_height* + (*count* - 1) <= *cp_height*

**Result**

> A dictionary with the following members:
>
> -   *count*
>
>     The number of headers returned, between zero and the number
>     requested. If the chain has not extended sufficiently far, only
>     the available headers will be returned. If more headers than *max*
>     were requested at most *max* will be returned.
>
> -   *hex*
>
>     The binary block headers concatenated together in-order as a
>     hexadecimal string. Starting with version 1.4.1, AuxPoW data (if
>     present in the original header) is truncated if *cp_height* is
>     nonzero.
>
> -   *max*
>
>     The maximum number of headers the server will return in a single
>     request.
>
> The dictionary additionally has the following keys if *count* and
> *cp_height* are not zero. This provides a proof that all the given
> headers are present in the blockchain; presumably the client has the
> merkle root hard-coded as a checkpoint.
>
> -   *root*
>
>     The merkle root of all blockchain headers up to and including
>     *cp_height*.
>
> -   *branch*
>
>     The merkle branch of the last returned header up to *root*,
>     deepest pairing first.

**Example Response**

See `here <cp_height example>` for an
example of *root* and *branch* keys.

    {
      "count": 2,
      "hex": "0100000000000000000000000000000000000000000000000000000000000000000000003ba3edfd7a7b12b27ac72c3e67768f617fc81bc3888a51323a9fb8aa4b1e5e4a29ab5f49ffff001d1dac2b7c010000006fe28c0ab6f1b372c1a6a246ae63f74f931e8365e15a089c68d6190000000000982051fd1e4ba744bbbe680e1fee14677ba1a3c3540bf7b1cdb606e857233e0e61bc6649ffff001d01e36299"
      "max": 2016
    }