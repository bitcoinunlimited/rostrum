## [blockchain.relayfee](site:/protocol/blockchain/blockchain-relayfee)

Return the minimum fee a low-priority transaction must pay in order to
be accepted to the daemon's memory pool.

**Signature**

> Function:
> blockchain.relayfee()

**Result**

> The fee in whole coin units (BTC, not satoshis for Bitcoin) as a
> floating point number.

**Example Results**

    1e-05

    0.0