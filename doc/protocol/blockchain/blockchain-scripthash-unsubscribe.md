## [blockchain.scripthash.unsubscribe](site:/protocol/blockchain/blockchain-scripthash-unsubscribe)

Unsubscribe from a script hash, preventing future notifications if its
`status` changes.

**Signature**

> Function:
> blockchain.scripthash.unsubscribe(scripthash)
>
> Version added:
> 1.4.2
>
> *scripthash*
>
> > The script hash as a hexadecimal string.

**Result**

> Returns `True` if the scripthash was
> subscribed to, otherwise `False`. Note
> that `False` might be returned even
> for something subscribed to earlier, because the server can drop
> subscriptions in rare circumstances.