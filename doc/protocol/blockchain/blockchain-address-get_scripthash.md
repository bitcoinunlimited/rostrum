## [blockchain.address.get\_scripthash](site:/protocol/blockchain/blockchain-address-get_scripthash)

Translate a Bitcoin Cash or a Nexa address to a [script hash](site:/protocol/basics). This method is
potentially useful for clients preferring to work with `script hashes` but
lacking the local libraries necessary to generate them.

**Signature**

> Function:
> blockchain.address.get\_scripthash(address)
>
> Version added:
> 1.4.3
>
--8<-- "doc/protocol/param_address.ext"

**Result**

> The unique 32-byte hex-encoded `script hash` that corresponds to the decoded address.