## [blockchain.address.get\_first\_use](site:/protocol/blockchain/blockchain-address-get_first_use)

Returns a first occurance of usage of scripthash as ouput on the blockchain.

See `blockchain.scripthash.get_first_use`.

**Signature**

>  Function: blockchain.scripthash.get\_first\_use(address, { "include_tokens": true, ...})
>
>  Version added: Added in Rostrum 1.2 (previously named ElectrsCash)
>
--8<-- "doc/protocol/param_address.ext"
--8<-- "doc/protocol/param_filter.ext"

**Result**

> See [blockchain.scripthash.get_first_use](site:/protocol/blockchain/blockchain-scripthash-get_first_use).