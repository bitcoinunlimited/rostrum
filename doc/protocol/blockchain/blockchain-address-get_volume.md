## [blockchain.address.get\_volume](site:/protocol/blockchain/blockchain-address-get_volume)

Return the confirmed and unconfirmed volume of a Bitcoin Cash address.

**Signature**

> Function:
> blockchain.address.get\_volume(address, { "include_tokens": true, ...})
>
> Version added:
> 11.1
>
--8<-- "doc/protocol/param_address.ext"
--8<-- "doc/protocol/param_filter_nooffsetlimit.ext"

**Result**

> See [blockchain.scripthash.get_volume](site:/protocol/blockchain/blockchain-scripthash-get_volume).
