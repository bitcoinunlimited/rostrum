## [blockchain.address.subscribe](site:/protocol/blockchain/blockchain-address-subscribe)

Subscribe to a Bitcoin Cash or Nexa address.

**Signature**

> Function:
> blockchain.address.subscribe(address)
>
> Version added:
> 1.4.3
>
--8<-- "doc/protocol/param_address.ext"

**Result**

> The `status` of the address.

**Notifications**

> As this is a subcription, the client will receive a notification when
> the `status` of the address changes. Its signature is
>
> Function:
> blockchain.address.subscribe(address, status)
>
|Note|
|----|
| The address returned back to the client when notifying of status changes will be in the same encoding and style as was provided when subscribing. In effect, a whitespace-stripped version of the address string that the client provided will be sent back to the client when notifying, in order to make it easier for clients to track the notification. |
| It is unspecified what happens if a client subscribes to the same address using multiple encodings or styles, but it is RECOMMENDED that servers simply update their internal subscription tables on subsequent subscriptions to the same destination such that they honor the latest subscription only, and not subscribe clients multiple times to the same logical destination. For example, Rostrum server will simply update its table for how to refer to the subscription and send clients subsequent notifications using the latest encoding style of that particular address that the client last provided. |
| Similarly, if a client mixes `blockchain.address.*` and `blockchain.scripthash.*` calls to the server, it is RECOMMENDED that the server treat all addresses as equivalent to their scripthashes internally such that it is possible to subscribe by address and later unsubscribe by scripthash, for example. |