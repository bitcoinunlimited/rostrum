## [blockchain.address.get\_mempool](site:/protocol/blockchain/blockchain-address-get_mempool)

Return the unconfirmed transactions of a Bitcoin Cash or Nexa address.

**Signature**

> Function:
> blockchain.address.get\_mempool(address, { "include\_tokens": true, ...})
>
> Version added: 1.4.3
>
--8<-- "doc/protocol/param_address.ext"
--8<-- "doc/protocol/param_filter_noheight.ext"


**Result**

> As for [blockchain.scripthash.get_mempool](site:/protocol/blockchain/blockchain-scripthash-get_mempool)
