## [blockchain.address.get\_balance](site:/protocol/blockchain/blockchain-address-get_balance)

Return the confirmed and unconfirmed balances of a Bitcoin Cash address.

**Signature**

> Function:
> blockchain.address.get\_balance(address, { "include_tokens": true, ...})
>
> Version added: 1.4.3
>
--8<-- "doc/protocol/param_address.ext"
--8<-- "doc/protocol/param_filter_noheight.ext"

**Result**

> See [blockchain.scripthash.get_balance](site:/protocol/blockchain/blockchain-scripthash-get_balance).
