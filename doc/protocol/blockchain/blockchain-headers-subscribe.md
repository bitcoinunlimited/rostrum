## [blockchain.headers.subscribe](site:/protocol/blockchain/blockchain-headers-subscribe)

Subscribe to receive block headers when a new block is found.

**Signature**

> Function:
> blockchain.headers.subscribe()

**Result**

> The header of the current block chain tip. The result is a dictionary
> with two members:
>
> -   *hex*
>
>     The binary header as a hexadecimal string.
>
> -   *height*
>
>     The height of the header, an integer.

**Example Result**

    {
      "height": 520481,
      "hex": "00000020890208a0ae3a3892aa047c5468725846577cfcd9b512b50000000000000000005dc2b02f2d297a9064ee103036c14d678f9afc7e3d9409cf53fd58b82e938e8ecbeca05a2d2103188ce804c4"
    }

**Notifications**

> As this is a subcription, the client will receive a notification when
> a new block is found. The notification's signature is:
>
> > Function:
> > blockchain.headers.subscribe(header)
> >
> > -   *header*
> >
> >     See **Result** above.

|Note|
|----|
|Should a new block arrive quickly, perhaps while the server is still processing prior blocks, the server may only notify of the most recent chain tip. The protocol does not guarantee notification of all intermediate block headers.|
|In a similar way the client must be prepared to handle chain reorganisations. Should a re-org happen the new chain tip will not sit directly on top of the prior chain tip. The client must be able to figure out the common ancestor block and request any missing block headers to acquire a consistent view of the chain state.|
|If a client subscribes multiple times, the subsequent subscriptions will be ignored and the client will continue to receive one notification per header.|

**See also**

Get tip without subscribing with [blockchain.headers.tip](site:/protocol/blockchain/blockchain-headers-tip)