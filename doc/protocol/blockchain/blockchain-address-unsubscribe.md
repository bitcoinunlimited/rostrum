## [blockchain.address.unsubscribe](site:/protocol/blockchain/blockchain-address-unsubscribe)

Unsubscribe from a Bitcoin Cash or Nexa address, preventing future notifications if its `status` changes.

**Signature**

> Function:
> blockchain.address.unsubscribe(address)
>
> Version added: 1.4.3
>
--8<-- "doc/protocol/param_address.ext"

**Result**

> Returns `True` if the address was
> subscribed to, otherwise `False`. Note
> that `False` might be returned even
> for something subscribed to earlier, because the server can drop
> subscriptions in rare circumstances.