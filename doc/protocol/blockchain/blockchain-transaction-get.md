## [blockchain.transaction.get](site:/protocol/blockchain/blockchain-transaction-get)

Return a raw transaction.

**Signature**

> Function:
> blockchain.transaction.get(tx\_hash, verbose=false)
>
> *tx_hash*
>
> > The transaction hash as a hexadecimal string.
> > On Nexa, this can be either the txid or txidem. However, if called by txidem,
> > then the node must have txindex enabled. Rostrum does not have an internal txidem index.
>
> *verbose*
>
> > Return a dictionary of a decoded transaction.

**Result**

=== "Bitcoin Cash"

    If *verbose* is `false`:  The raw transaction as a hexadecimal string.
    If *verbose* is `true`: The result is a dictionary of a decoded transaction.

    If transaction does not exist on blockhain or in the mempool, an error is returned.

        {
          "hex" : "data",       (string) The serialized, hex-encoded data for 'txid'
          "txid" : "id",        (string) The transaction id (same as provided)
          "hash" : "id",        (string) The transaction hash
          "size" : n,             (numeric) The serialized transaction size
          "version" : n,          (numeric) The version
          "locktime" : ttt,       (numeric) The lock time
          "vin" : [               (array of json objects)
             {
               "coinbase":        (string|null) Contains scriptsig hex when coinbase input
               "txid": "id",      (string) The transaction id
               "vout": n,         (numeric)
               "scriptSig": {     (json object) The script
                 "asm": "asm",    (string) asm
                 "hex": "hex"     (string) hex
               },
               "sequence": n      (numeric) The script sequence number
               "value" : x.xxx,     (numeric) The input value in BCH
               "value_coins" : x.xxx,     (numeric) The input value in BCH
               "value_satoshi" : n,   (numeric) The input value in BCH in satoshis
             }
             ,...
          ],
          "vout" : [              (array of json objects)
             {
               "value" : x.xxx,            (numeric) The output value in BCH
               "value_coins" : x.xxx,      (numeric) The output value in BCH
               "value_satoshi" : n,        (numeric) The output value in BCH in satoshis
               "n" : n,                    (numeric) index
               "scriptPubKey" : {          (json object)
                 "asm" : "asm",          (string) the asm
                 "hex" : "hex",          (string) the hex
                 "type" : "pubkeyhash",  (string) The type, eg 'pubkeyhash'
                 "addresses" : [           (json array of string)
                   "address"        (string) Bitcoin Cash address
                   ,...
                 ]
               },
               "tokenData" : {             (json object optional)
                 "category" : "hex",       (string) token id
                 "amount" : "xxx",         (string) fungible amount (is a string to support >53-bit amounts)
                 "nft" : {                 (json object optional)
                   "capability" : "xxx",   (string) one of "none", "mutable", "minting"
                   "commitment" : "hex"    (string) NFT commitment
                 }
               }
             }
             ,...
          ],
          "blockhash" : "hash",   (string) the block hash
          "confirmations" : n,      (numeric) The confirmations
          "time" : ttt,             (numeric) The transaction time in seconds since epoch (Jan 1 1970 GMT)
          "blocktime" : ttt,        (numeric) The block time in seconds since epoch (Jan 1 1970 GMT)
          "fee" : x.xxx,            (numeric) Transaction fee in BCH
          "fee_satoshi" : n,        (numeric) Transaction fee in BCH in satoshis
        }


=== "Nexa"

    If *verbose* is `false`:  The raw transaction as a hexadecimal string.
    If *verbose* is `true`: The result is a dictionary of a decoded transaction.

    If transaction does not exist on blockhain or in the mempool, an error is returned.

        {
          "hex" : "data",       (string) The serialized, hex-encoded data for 'txid'
          "txid" : "id",        (string) The transaction id (same as provided)
          "txidem": "idem",     (string) The transaction idem
          "hash" : "id",        (string) The transaction hash
          "size" : n,             (numeric) The serialized transaction size
          "version" : n,          (numeric) The version
          "locktime" : ttt,       (numeric) The lock time
          "vin" : [               (array of json objects)
             {
               "scriptSig": {     (json object) The script
                 "asm": "asm",    (string) asm
                 "hex": "hex"     (string) hex
               },
               "sequence": n      (numeric) The script sequence number
               "value" : x.xxx,     (numeric) The input value in NEX
               "value_coins" : x.xxx,     (numeric) The input value in NEX
               "value_satoshi" : n,   (numeric) The input value in NEX in satoshis
               "outpoint" "hash"  (string) The outpoint hash
               "addresses" : [           (json array of string)
                  "address"        (string) Nexa address
                  ,...
                ]
               "group": "token"       (string) The group ID encoded as cashaddr
               "groupQuantity": n     (numeric|null) The output value of group tokens
               "groupAuthority": n    (numeric|null) The group authority bits
             }
             ,...
          ],
          "vout" : [              (array of json objects)
             {
               "value" : x.xxx,            (numeric) The output value in NEX
               "value_coins" : x.xxx,      (numeric) The output value in NEX
               "value_satoshi" : n,        (numeric) The output value in NEX in satoshis
               "n" : n,                    (numeric) index
               "scriptPubKey" : {          (json object)
                 "asm" : "asm",          (string) the asm
                 "hex" : "hex",          (string) the hex
                 "type" : "pubkeyhash",  (string) The type, eg 'pubkeyhash'
                 "addresses" : [           (json array of string)
                   "address"        (string) Nexa address
                   ,...
                 ]
               },
               "group": "token"       (string) The group ID encoded as cashaddr
               "groupQuantity": n     (numeric|null) The output value of group tokens
               "groupAuthority": n    (numeric|null) The group authority bits
               "token_id_hex": "id"   (string) The group ID as hex
               "scriptHash": "scripthash" (string|null) The scripthash of script template
               "argsHash": "argshash" (string|null) The argument hash of script template
               "outpoint_hash": "hex" (string) Hash of utxo (hash of transaction idem + output index)
             }
             ,...
          ],
          "blockhash" : "hash",   (string) the block hash
          "confirmations" : n,      (numeric) The confirmations
          "time" : ttt,             (numeric) The transaction time in seconds since epoch (Jan 1 1970 GMT)
          "blocktime" : ttt,        (numeric) The block time in seconds since epoch (Jan 1 1970 GMT)
          "fee" : x.xxx,            (numeric) Transaction fee in NEX
          "fee_satoshi" : n,        (numeric) Transaction fee in NEX in satoshis
        }


**Example Results**

When *verbose* is `false`:

    "01000000015bb9142c960a838329694d3fe9ba08c2a6421c5158d8f7044cb7c48006c1b48"
    "4000000006a4730440220229ea5359a63c2b83a713fcc20d8c41b20d48fe639a639d2a824"
    "6a137f29d0fc02201de12de9c056912a4e581a62d12fb5f43ee6c08ed0238c32a1ee76921"
    "3ca8b8b412103bcf9a004f1f7a9a8d8acce7b51c983233d107329ff7c4fb53e44c855dbe1"
    "f6a4feffffff02c6b68200000000001976a9141041fb024bd7a1338ef1959026bbba86006"
    "4fe5f88ac50a8cf00000000001976a91445dac110239a7a3814535c15858b939211f85298"
    "88ac61ee0700"

When *verbose* is `true` on Bitcoin Cash:

    {
      "blockhash": "00000000000000000389720f75be0c139b11307d258ef20461d3829a0b6eaa5a",
      "blocktime": 1609802384,
      "confirmations": 97828,
      "hash": "513a9982488969ff80204b1ae6b5135b9e28516c52f658dc64b607b521af8c78",
      "height": 668904,
      "hex": "01000000017c7644abb7c34294416307c5010bd9077d75ea93930ea19d888c229ce136a11201000000644151fe1d606f3e02ebed01d436429229ec199b47c67220ae58fbf034d3b6e2f90e263477a925f5ba9cd98cadb0cf7193608da6a5cd597b43fa71adc0631441f168412103dae2be8889698de9913ca48a394e6dc49cfcc47919139b2f74dd05b8058731defeffffff030000000000000000136a11424553542052454345495054204556455275380000000000001976a9143dcff26ceba80f2aa0c5e762964f50002ba137bc88ac7c390000000000001976a914f40a3047ee2b1efc3c89072d798b9c3cb4fa374d88ace7340a00",
      "locktime": 668903,
      "size": 247,
      "time": 1609802384,
      "txid": "513a9982488969ff80204b1ae6b5135b9e28516c52f658dc64b607b521af8c78",
      "version": 1,
      "vin": [
        {
          "coinbase": null,
          "scriptSig": {
            "asm": "OP_PUSHBYTES_65 51fe1d606f3e02ebed01d436429229ec199b47c67220ae58fbf034d3b6e2f90e263477a925f5ba9cd98cadb0cf7193608da6a5cd597b43fa71adc0631441f16841 OP_PUSHBYTES_33 03dae2be8889698de9913ca48a394e6dc49cfcc47919139b2f74dd05b8058731de",
            "hex": "4151fe1d606f3e02ebed01d436429229ec199b47c67220ae58fbf034d3b6e2f90e263477a925f5ba9cd98cadb0cf7193608da6a5cd597b43fa71adc0631441f168412103dae2be8889698de9913ca48a394e6dc49cfcc47919139b2f74dd05b8058731de"
          },
          "sequence": 4294967294,
          "txid": "12a136e19c228c889da10e9393ea757d07d90b01c50763419442c3b7ab44767c",
          "vout": 1
          "value_coin": 0.00014453,
          "value_satoshi": 14453
        }
      ],
      "vout": [
        {
          "n": 0,
          "scriptPubKey": {
            "addresses": [],
            "asm": "OP_RETURN OP_PUSHBYTES_17 4245535420524543454950542045564552",
            "hex": "6a114245535420524543454950542045564552",
            "type": "nulldata"
          },
          "value": 0.0,
          "value_coin": 0.0,
          "value_satoshi": 0
        },
        {
          "n": 1,
          "scriptPubKey": {
            "addresses": [
              "bitcoincash:qq7ulunvaw5q724qchnk99j02qqzhgfhhsa7twn8mt"
            ],
            "asm": "OP_DUP OP_HASH160 OP_PUSHBYTES_20 3dcff26ceba80f2aa0c5e762964f50002ba137bc OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a9143dcff26ceba80f2aa0c5e762964f50002ba137bc88ac",
            "type": "pubkeyhash"
          },
          "value": 0.00014453,
          "value_coin": 0.00014453,
          "value_satoshi": 14453
        },
        {
          "n": 2,
          "scriptPubKey": {
            "addresses": [
              "bitcoincash:qr6q5vz8ac43alpu3yrj67vtns7tf73hf5073aed46"
            ],
            "asm": "OP_DUP OP_HASH160 OP_PUSHBYTES_20 f40a3047ee2b1efc3c89072d798b9c3cb4fa374d OP_EQUALVERIFY OP_CHECKSIG",
            "hex": "76a914f40a3047ee2b1efc3c89072d798b9c3cb4fa374d88ac",
            "type": "pubkeyhash"
          },
          "value": 0.00014716,
          "value_coin": 0.00014716,
          "value_satoshi": 14716
        }
      ]
    }
