## [blockchain.scripthash.get\_volume](site:/protocol/blockchain/blockchain-scripthash-get_volume)

Return the confirmed and unconfirmed volume of a `script hash`.

**Signature**

> Function:
> blockchain.scripthash.get\_volume(scripthash, { "include_tokens": true, ...})
>
> Version added:
> 11.1
>
> -   *scripthash*
>
>     The script hash as a hexadecimal string.
>
--8<-- "doc/protocol/param_filter_nooffsetlimit.ext"


**Result**

> A dictionary with keys `confirmed_sats_in`, `confirmed_sats_out`, `mempool_sats_in`, `mempool_sats_out`.
> The value of each is the appropriate volume in satoshis.

**Result Example**

    {
      'confirmed_sats_in': 0,
      'confirmed_sats_out': 0,
      'mempool_sats_in': 600,
      'mempool_sats_out': 400
    }
