## [blockchain.transaction.get\_confirmed\_blockhash](site:/protocol/blockchain/blockchain-transaction-get_confirmed_blockhash)

Returns the blockhash of a block the transaction confirmed in. Returns error
if transaction is not confirmed (or does not exist).

**Signature**

> Function:
> blockchain.transaction.get\_confirmed\_blockhash(tx\_hash)`
>>
> *tx_hash*
>
> > The transaction hash as a hexadecimal string.

**Result**

> A dictionary with the following keys:
>
> -   *block_hash*
>
>     The hash of the block in which the transaction was confirmed.
>
> -   *block_height*
>
>     The height of the block in which the transaction was confirmed.


**Example result**
```
{
'block_hash': '000000000000000002a04f56505ef459e1edd21fb3725524116fdaedf3a4d0ab',
'block_height': 597843,
}
```