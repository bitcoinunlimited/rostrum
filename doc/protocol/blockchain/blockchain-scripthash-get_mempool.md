## [blockchain.scripthash.get\_mempool](site:/protocol/blockchain/blockchain-scripthash-get_mempool)

Return the unconfirmed transactions of a `script hash <script hashes>`.

**Signature**

> Function:
> blockchain.scripthash.get\_mempool(scripthash, { "include_tokens": true, ...})
>
> Version added:
> 1.1
>
> -   *scripthash*
>
> The script hash as a hexadecimal string.
>
--8<-- "doc/protocol/param_filter_noheight.ext"


**Result**

> A list of mempool transactions in arbitrary order. Each mempool
> transaction is a dictionary with the following keys:
>
> -   *height*
>
>     `0` if all inputs are confirmed, and `-1` otherwise.
>
> -   *tx_hash*
>
>     The transaction hash in hexadecimal.
>
> -   *fee*
>
>     The transaction fee in minimum coin units (satoshis).

**Result Example**

    [
      {
        "tx_hash": "45381031132c57b2ff1cbe8d8d3920cf9ed25efd9a0beb764bdb2f24c7d1c7e3",
        "height": 0,
        "fee": 24310
      }
    ]
