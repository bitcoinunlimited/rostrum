## [blockchain.block.header_verbose](site:/protocol/blockchain/blockchain-block-header_verbose)

Return the block header at the given height with all fields decoded.

**Signature**

> Function:
> blockchain.block.header_verbose(height_or_hash)
>
> Version added:
> Rostrum 10.1
>
> > *height_or_hash*
> >
> > The height of the block, a non-negative integer, or a blockhash.
>

**Result**

A block header with all of its fields decoded, in addition to median time past and header hex.


**Example Result**

=== "Bitcoin Cash"

        {
          'bits': 545259519,
          'hash': '6f5b55854744733d8c1a99aef931c3b6f031f1bfb0428845a1849b80fc1b3ded',
          'height': 111,
          'hex': '00000020710155a76704894788fb8cca9ecac8805f3d0a734a1ee6d35dd625ff67dd6b4851a1389e8ef78148c9c7e965509125139222bda371dd2f9518eb196bd52bc6c60dedb065ffff7f2000000000',
          'mediantime': 1706093836,
          'merkleroot': 'c6c62bd56b19eb18952fdd71a3bd22921325915065e9c7c94881f78e9e38a151',
          'nonce': 0,
          'previousblockhash': '486bdd67ff25d65dd3e61e4a730a3d5f80c8ca9eca8cfb8847890467a7550171',
          'time': 1706093837,
          'version': 536870912
        }

=== "Nexa"

        {
          'ancestorhash': 'd71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371',
          'bits': 545259519,
          'chainwork': '00000000000000000000000000000000000000000000000000000000000000e0',
          'feepoolamt': 0,
          'hash': '08abee10a905a482d40339aa4cc99902b4a201b19281ba23d4fcf64a0ec62e85',
          'height': 111,
          'hex': 'd14b2a495d2a38651f66c199b0e9680c59e4852227cfdc65d8b8a6cc47dfe502ffff7f2071b3d0c9122622e3045c15b60e2c65d5f171e0216b1af3fe2dd107e331e41ed7680fe515f6314078bd90009de3f5f456ef8c2b4c87e76a5fa6da14b784ae5a2b0000000000000000000000000000000000000000000000000000000000000000d3ecb0656fe000000000000000000000000000000000000000000000000000000000000000bd2b000000000000330000000405000000',
          'mediantime': 1706093778,
          'merkleroot': '2b5aae84b714daa65f6ae7874c2b8cef56f4f5e39d0090bd784031f615e50f68',
          'minerdata': '',
          'nonce': '05000000',
          'previousblockhash':
          '02e5df47cca6b8d865dccf272285e4590c68e9b099c1661f65382a5d492a4bd1',
          'size': 11197,
          'time': 1706093779,
          'txcount': 51,
          'txfilter': '0000000000000000000000000000000000000000000000000000000000000000',
          'utxocommitment': ''
        }

