## [blockchain.scripthash.get\_balance](site:/protocol/blockchain/blockchain-scripthash-get_balance)

Return the confirmed and unconfirmed balances of a `script hash`.

**Signature**

> Function:
> blockchain.scripthash.get\_balance(scripthash, { "include_tokens": true, ...})
>
> Version added:
> 1.1
>
> -   *scripthash*
>
>     The script hash as a hexadecimal string.
>
--8<-- "doc/protocol/param_filter_noheight.ext"


**Result**

> A dictionary with keys `confirmed` and `unconfirmed`. The value of each is the appropriate
> balance in satoshis.

**Result Example**

    {
      "confirmed": 103873966,
      "unconfirmed": 236844
    }
