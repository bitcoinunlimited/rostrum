## [blockchain.address.get\_history](site:/protocol/blockchain/blockchain-address-get_history)

Return the confirmed and unconfirmed history of a Bitcoin Cash or Nexa address.

**Signature**

> Function:
> blockchain.address.get\_history(address, { "include_tokens": true, ...})
>
> Version added: 1.4.3
>
--8<-- "doc/protocol/param_address.ext"
--8<-- "doc/protocol/param_filter.ext"


**Result**

> See [blockchain.scripthash.get_history](site:/protocol/blockchain/blockchain-scripthash-get_history)
