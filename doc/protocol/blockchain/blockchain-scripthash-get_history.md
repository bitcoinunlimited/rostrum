## [blockchain.scripthash.get\_history](site:/protocol/blockchain/blockchain-scripthash-get_history)

Return the confirmed and unconfirmed history of a `script hash`

**Signature**

> Function:
> blockchain.scripthash.get\_history(scripthash, { "include_tokens": true, ...})
>
> Version added:
> 1.1
>
> -   *scripthash*
>
> The script hash as a hexadecimal string.
>
--8<-- "doc/protocol/param_filter.ext"


**Result**

> A list of confirmed transactions in blockchain order, with the output
> of `blockchain.scripthash.get_mempool`
> appended to the list. Each confirmed transaction is a dictionary with
> the following keys:
>
> -   *height*
>
>     The integer height of the block the transaction was confirmed in.
>
> -   *tx_hash*
>
>     The transaction hash in hexadecimal.
>
> See `blockchain.scripthash.get_mempool`
> for how mempool transactions are returned.

**Result Examples**

    [
      {
        "height": 200004,
        "tx_hash": "acc3758bd2a26f869fcc67d48ff30b96464d476bca82c1cd6656e7d506816412"
      },
      {
        "height": 215008,
        "tx_hash": "f3e1bf48975b8d6060a9de8884296abb80be618dc00ae3cb2f6cee3085e09403"
      }
    ]

    [
      {
        "fee": 20000,
        "height": 0,
        "tx_hash": "9fbed79a1e970343fcd39f4a2d830a6bde6de0754ed2da70f489d0303ed558ec"
      }
    ]
