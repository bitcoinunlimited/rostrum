## [blockchain.scripthash.listunspent](site:/protocol/blockchain/blockchain-scripthash-listunspent)

Return an ordered list of UTXOs sent to a script hash.

**Signature**

> Function:
> blockchain.scripthash.listunspent(scripthash, { "include_tokens": true, ...})
>
> Version added:
> 1.1
>
> -   *scripthash*
>
> The script hash as a hexadecimal string.
>
--8<-- "doc/protocol/param_filter.ext"


**Result**

> A list of unspent outputs in blockchain order. This function takes the
> mempool into account. Mempool transactions paying to the address are
> included at the end of the list in an undefined order. Any output that
> is spent in the mempool does not appear. Each output is a dictionary
> with the following keys:
>
> -   *height*
>
>     The integer height of the block the transaction was confirmed in.
>     `0` if the transaction is in the mempool.
>
> -   *tx_pos*
>
>     The zero-based index of the output in the transaction's list of
>     outputs.
>
> -   *tx_hash*
>
>     The output's transaction hash as a hexadecimal string. The hash
>     is little-endian encoded (same as bitcoind RPC).
>
> -   *value*
>
>     The output's value in minimum coin units (satoshis).
>
> -   *outpoint_hash*
>
>     Hash of utxo (hash of transaction idem + output index)

**Result Example**

    [
      {
        "tx_pos": 0,
        "value": 45318048,
        "tx_hash": "9f2c45a12db0144909b5db269415f7319179105982ac70ed80d76ea79d923ebf",
        "height": 437146,
        "outpoint_hash": "fbd96943e964abb082845fd67e89e2689e649d9ddec496605d214c131e91025e"
      },
      {
        "tx_pos": 0,
        "value": 919195,
        "tx_hash": "3d2290c93436a3e964cfc2f0950174d8847b1fbe3946432c4784e168da0f019f",
        "height": 441696,
        "outpoint_hash": "65a867e6d7da3f3986f8fe67fda311a60ac5c9b43e9f28326646f0d3f10381aa"
      }
    ]
