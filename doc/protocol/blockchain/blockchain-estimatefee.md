## [blockchain.estimatefee](site:/protocol/blockchain/blockchain-estimatefee)

Return the estimated transaction fee per kilobyte for a transaction to
be confirmed within a certain number of blocks.

**Signature**

> Function:
> blockchain.estimatefee(number)
> :::
>
> *number*
>
> > The number of blocks to target for confirmation.

**Result**

> The estimated transaction fee in coin units per kilobyte, as a
> floating point number. If the server does not have enough information
> to make an estimate, the integer `-1` is returned.

**Example Result**

    0.00101079