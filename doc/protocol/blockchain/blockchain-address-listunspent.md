## [blockchain.address.listunspent](site:/protocol/blockchain/blockchain-address-listunspent)

Return an ordered list of UTXOs sent to a Bitcoin Cash or Nexa address.

**Signature**

> Function:
> blockchain.address.listunspent(address, { "include\_tokens": true, ...})
>
> Version added:
> 1.4.3
>
--8<-- "doc/protocol/param_address.ext"
--8<-- "doc/protocol/param_filter.ext"


**Result**

> As for [blockchain.scripthash.listunspent](site:/protocol/blockchain/blockchain-scripthash-listunspent)
