# Installation

## Latest unsigned binaries

The latest binaries built by GitLab continuous integration.

**[Go to downloads page](https://nexa.gitlab.io/rostrum/nightly).**

Binaries for Bitcoin Cash and Nexa are compiled for these platforms:

* GNU/Linux x86-64
* GNU/Linux ARM64
* Windows x86-64

Note that these are the latest build of the git repository.

Example for downloading via terminal:
```bash
$ wget https://nexa.gitlab.io/rostrum/nightly/rostrum-bitcoincash-linux-x86-64-6.0.0-2a0d6d6.gz
$ gzip -d rostrum-bitcoincash-linux-x86-64-6.0.0-2a0d6d6.gz
$ chmod +x rostrum
$ ./rostrum
```

## Signed binaries

Pre-built binaries exist for 64-bit GNU/Linux. For other platforms, you need
to install from cargo or source. These are the latest release of Rostrum that was
available at the time of the Nexa or Bitcoin Unlimited release. These are
deterministically built and signed by multiple individuals.

* [Bitcoin Unlimited releases](https://www.bitcoinunlimited.info/download)
* [Nexa releases](https://www.nexa.org/node).


## Install using cargo

Get cargo by installing a recent version of Rust. The tool [rustup](https://rustup.rs) is a good tool for installing Rust.

Run `cargo install rostrum --features=bch --locked` to install the latest release for Bitcoin Cash or
`cargo install rostrum --features=nexa --locked` for Nexa. This will build and install
the latest version from [crates.io](https://crates.io/crates/rostrum).

Rostrum uses RocksDB and the requires clang and cmake. See [build documentation](build.md) to see how to install these packages on your platform.

## Building from source

See [build documentation](build.md).

## Install using Docker

You can run `rostrum` via docker using the images provided by nexa developers. You can find the repository for the docker files at https://gitlab.com/nexa/docker-nexa/-/tree/master/nexad-rostrum

The images and instructions on how to use it are are published at https://hub.docker.com/r/bchunlimited/nexad-rostrum


