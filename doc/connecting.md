# Connecting to Rostrum

It's possible to connect to rostrum over raw TCP connection, websocket and HTTP.
Additionally, encrypted connections are supported using [reverse proxy](ssl.md).

For websocket, see [websocket documentation](websockets.md) and for HTTP see [HTTP documentation](httpconnection.md).

For TCP, see below.

If you want to use it from a terminal, you can use the `contrib/client.py` script from the Rostrum source code repostiory.

## Wallet: Electron Cash

```bash
# Connecting to your local server
$ electron-cash --oneserver --server=127.0.0.1:50001:t
```

Note: Some versions of Electron Cash have an issue where it won't connect to non-SSL server, even when it's localhost.


## Terminal

### Using Python over TCP

Using the `contrib/client.py` script from the Rostrum source code repostiory.

```bash
$ python3 contrib/client.py -h
usage: client.py [-h] [--port PORT] [--server SERVER] method [args ...]

positional arguments:
  method
  args

optional arguments:
  -h, --help       show this help message and exit
  --port PORT
  --server SERVER
  ```

  Example:
  ```bash
% python3 contrib/client.py blockchain.block.headers 0 1
{'id': 0, 'jsonrpc': '2.0', 'result': {'count': 1, 'hex': '0100000000000000000000000000000000000000000000000000000000000000000000003ba3edfd7a7b12b27ac72c3e67768f617fc81bc3888a51323a9fb8aa4b1e5e4a29ab5f49ffff001d1dac2b7c', 'max': 2016}}
```

### Using node.js over SSL

The package [electrum-cli](https://www.npmjs.com/package/electrum-cli) is a command line tool for querying elecrum servers over a SSL connection.

Example:
```
% electrum-cli --server rostrum.nexa.ink:20002 server.version
[
  "Rostrum 6.0.0",
  "1.4"
]
```

### Using netcat over TCP

It's possible to use [ncat](https://nmap.org/ncat/) to do simple queries from
terminal. For Nexa, use port 20001. For Bitcoin Cash, use port 50001.

```bash
% (echo '{ "id": 1, "method": "server.version", "params": ["AtomicDEX", ["1.4", "2.0"]] }'; sleep 0.5) | ncat rostrum.devops.cash 20001
{"id":1,"jsonrpc":"2.0","result":["Rostrum 5.0.0","1.4.3"]}

```

## JavaScript / TypeScript: ElectrumCash library

```typescript
import { ElectrumCluster, ElectrumTransport, RequestResponse } from 'electrum-cash';

const electrum = new ElectrumCluster('client name', '1.4.2', 1, 1);
electrum.addServer('bitcoincash.network');
try {
  await electrum.ready();
} catch (e) {
  console.log('Failed to connect ', e);
}
const response = await electrum.request(
    "blockchain.block.headers", 0, 1);
await electrum.shutdown();
```

### Using Python over Web Socket (wss or ws)

Using the `contrib/websocket-client.py` script from the Rostrum source code repository.

```bash
$ python3 contrib/websocket-client.py -h
usage: _wsdump.py [-h] [-p PROXY] [-v [VERBOSE]] [-n] [-r] [-s [SUBPROTOCOLS ...]] [-o ORIGIN] [--eof-wait EOF_WAIT] [-t TEXT] [--timings] [--headers HEADERS] ws_url

WebSocket Simple Dump Tool

positional arguments:
  ws_url                websocket url. ex. ws://echo.websocket.events/

options:
  -h, --help            show this help message and exit
  -p PROXY, --proxy PROXY
                        proxy url. ex. http://127.0.0.1:8080
  -v [VERBOSE], --verbose [VERBOSE]
                        set verbose mode. If set to 1, show opcode. If set to 2, enable to trace websocket module
  -n, --nocert          Ignore invalid SSL cert
  -r, --raw             raw output
  -s [SUBPROTOCOLS ...], --subprotocols [SUBPROTOCOLS ...]
                        Set subprotocols
  -o ORIGIN, --origin ORIGIN
                        Set origin
  --eof-wait EOF_WAIT   wait time(second) after 'EOF' received.
  -t TEXT, --text TEXT  Send initial text
  --timings             Print timings in seconds
  --headers HEADERS     Set custom headers. Use ',' as separator
  ```

Example:

```bash
  % python3 contrib/websocket-client.py blockchains://electrum.nexa.org:20004 -t '{"id":0, "method":"blockchain.transaction.get", "params":[ "868d9ae77bb08def01d111f63f9e220bb24f0350379072418d08ae98db2650c3"]}'
  Press Ctrl+C to quit
  < {"id":0,"jsonrpc":"2.0","result":"000200ef80ade09d5ff63919f528f43315f461bd3e869a5e66ce39629d97393c0d316d64222103d1becb4798abb1d016be6889cbb0ba0b19c9f1d5e07961779c089af3c63984bd401a8c0fb4e65a9cfac06303c1256637882e8c0ec39dba13fd955c4306af5a29edbba54849bb750168662d75370d0a71a6482824ba3fbf6f70165c55d85770d8a7ffffffffc98d00000000000000dc5c3970d0309c636634a5a5e0b8d55980dc02cc92b765e5920b868771c109a064222103d1becb4798abb1d016be6889cbb0ba0b19c9f1d5e07961779c089af3c63984bd401a8c0fb4e65a9cfac06303c1256637882e8c0ec39dba13fd955c4306af5a29edbba54849bb750168662d75370d0a71a6482824ba3fbf6f70165c55d85770d8a7ffffffff580200000000000002015802000000000000170051142a5ad477aae98a85c03c2b641656e6f0b37e999d015c8c00000000000017005114375f5e5be9ddf719d8635306663c7903b530dbab00000000"}
  >
```
