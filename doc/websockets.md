# Websockets

Plain text websockets run on port 50003 for BCH and 20003 for Nexa. The default port to use for [encrypted connection (wss)](ssl.md) is 50004 for BCH and 20004 for Nexa.

## JavaScript / TypeScript

[electrum-cash](https://www.npmjs.com/package/electrum-cash) is a decent library for working with electrum web sockets.

Example usage: <https://gitlab.com/dagurval/flipstarters/-/blob/master/src/electrum.ts>


## Example request

**Request:**

```
{"method":"blockchain.transaction.get","params":["ef6d2825e320a7e738240de3e8c7cec02aa86506baf3b142ed3e48d4614bd20a",true],"id":194}

```

Notice the newline.

**Response:**

```
{
    "id": 194,
    "jsonrpc": "2.0",
    "result": {
        "blockhash": "000000000000000005489fc85f7244647ea0a28542f79a33167b38e1e205b441",
        "blocktime": 1647108743,
        "confirmations": 23671,
        "hash": "ef6d2825e320a7e738240de3e8c7cec02aa86506baf3b142ed3e48d4614bd20a",
        "height": 731035,
        "hex": "0100000001ae7a7bb1dd88c9d2ec81a311a584cd6edefc9e74c4543c5ff9cb7d331107446e2f0000006441339bf3d12cea59f2305bdab15fadd2034c599b63429e6b5b567b420b29720255dd957ff1476e6c323f11fc3f91682a7a009de856e83f1bfa05f6d8d812b69bc9412103c729493827c3d84edab740162e9ade303b33d687ce352539a24773200a7b48a2feffffff0249fa2a67000000001976a91427c44ecd750fb9a8c0a2119d95b9c1efa9502e7b88ac3a26b770000000001976a914efad7f10ac64f8f4f8f953d965c688f9583e858e88ac9a270b00",
        "locktime": 731034,
        "size": 219,
        "time": 1647108743,
        "txid": "ef6d2825e320a7e738240de3e8c7cec02aa86506baf3b142ed3e48d4614bd20a",
        "version": 1,
        "vin": [
            {
                "coinbase": null,
                "scriptSig": {
                    "asm": "OP_PUSHBYTES_65 339bf3d12cea59f2305bdab15fadd2034c599b63429e6b5b567b420b29720255dd957ff1476e6c323f11fc3f91682a7a009de856e83f1bfa05f6d8d812b69bc941 OP_PUSHBYTES_33 03c729493827c3d84edab740162e9ade303b33d687ce352539a24773200a7b48a2",
                    "hex": "41339bf3d12cea59f2305bdab15fadd2034c599b63429e6b5b567b420b29720255dd957ff1476e6c323f11fc3f91682a7a009de856e83f1bfa05f6d8d812b69bc9412103c729493827c3d84edab740162e9ade303b33d687ce352539a24773200a7b48a2"
                },
                "sequence": 4294967294,
                "txid": "6e440711337dcbf95f3c54c4749efcde6ecd84a511a381ecd2c988ddb17b7aae",
                "vout": 47
            }
        ],
        "vout": [
            {
                "n": 0,
                "scriptPubKey": {
                    "addresses": [
                        "bitcoincash:qqnugnkdw58mn2xq5ggem9dec8h6j5pw0vet778mv4"
                    ],
                    "asm": "OP_DUP OP_HASH160 OP_PUSHBYTES_20 27c44ecd750fb9a8c0a2119d95b9c1efa9502e7b OP_EQUALVERIFY OP_CHECKSIG",
                    "hex": "76a91427c44ecd750fb9a8c0a2119d95b9c1efa9502e7b88ac",
                    "type": "pubkeyhash"
                },
                "value_coin": 17.30869833,
                "value_satoshi": 1730869833
            },
            {
                "n": 1,
                "scriptPubKey": {
                    "addresses": [
                        "bitcoincash:qrh66lcs43j03a8cl9fajewx3ru4s0593cwk7wdqvx"
                    ],
                    "asm": "OP_DUP OP_HASH160 OP_PUSHBYTES_20 efad7f10ac64f8f4f8f953d965c688f9583e858e OP_EQUALVERIFY OP_CHECKSIG",
                    "hex": "76a914efad7f10ac64f8f4f8f953d965c688f9583e858e88ac",
                    "type": "pubkeyhash"
                },
                "value_coin": 18.91051066,
                "value_satoshi": 1891051066
            }
        ]
    }
}
```


## Python

[websocket-client](https://pypi.org/project/websocket-client/) is a popular library for working with web sockets in Python.


## Example usage


```Python
from websocket import create_connection
from time import sleep
import websocket 
import json

ws = create_connection("wss://bitcoincash.network:50004/") 

content = {
    "method": "blockchain.transaction.get",
    "params": ["ef6d2825e320a7e738240de3e8c7cec02aa86506baf3b142ed3e48d4614bd20a", True],
    "id": 0
}

ws.send(json.dumps(content).encode('utf-8')+b'\n')
sleep(0.5)

result =  ws.recv()

print(result)
ws.close()
```

**Response:**

Will be same as the response to the Javascript request above.
