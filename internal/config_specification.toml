[general]
env_prefix = "ROSTRUM"
conf_file_param = "conf"
conf_dir_param = "conf_dir"
doc = """
Rostrum is an efficient implementation of Electrum Server and can be used
as a drop-in replacement for ElectrumX. In addition to the TCP RPC interface,
it also provides WebSocket support.

Rostrum fully implements the version 1.4.3 of the Electrum Cash protocol
and in addition to useful extensions, including CashAccounts.

The server indexes the entire Bitcoin Cash blockchain, and the resulting index
enables fast queries for blockchain applications and any given user wallet,
allowing the user to keep real-time track of his balances and his transaction
history.

When run on the user's own machine, there is no need for the wallet to
communicate with external Electrum servers,
thus preserving the privacy of the user's addresses and balances."""

[[switch]]
name = "verbose"
abbr = "v"
doc = "Increase logging verbosity"
count = true

[[switch]]
name = "timestamp"
doc = "Prepend log lines with a timestamp"

[[param]]
name = "db_dir"
type = "std::path::PathBuf"
doc = "Directory to store index database (default: ./db/)"
default = "\"./db\".into()"

[[param]]
name = "db_write_cache_entries"
type = "usize"
doc = "How many database entries to cache in memory before flushing to disk during indexing. If `low_memory` is enabled, then this cache is disabled."
default = "500_000"

[[param]]
name = "daemon_dir"
type = "std::path::PathBuf"
doc = "Data directory of full node (default: ~/.bitcoin/ or ~/.nexa)"
default = "crate::config::default_daemon_dir()"

[[param]]
name = "cookie"
type = "String"
doc = "DEPRECATED: use cookie_file or auth instead!"

[[param]]
name = "auth"
type = "String"
doc = "JSONRPC authentication ('USER:PASSWORD', default: use cookie file)"

[[param]]
name = "cookie_file"
type = "std::path::PathBuf"
doc = "JSONRPC authentication cookie file (default: ~/.bitcoin/.cookie or ~/.nexa/.cookie)"
# This is safe to configure on command line.

[[param]]
name = "network"
type = "crate::config::BitcoinNetwork"
convert_into = "::bitcoincash::network::constants::Network"
doc = "Select Bitcoin network type ('bitcoin' for mainnet, 'testnet', 'testnet4' BCH only, 'scalenet' BCH only, 'chipnet' BCH only or 'regtest')"
default = "Default::default()"

[[param]]
name = "electrum_rpc_addr"
type = "crate::config::ResolvAddr"
doc = "Electrum server JSONRPC 'addr:port' to listen on (default: '0.0.0.0:50001' or 20001 (NEXA) for mainnet, '0.0.0.0:60001' or 30001 for testnet and '0.0.0.0:60401' or 30401 for regtest)"

[[param]]
name = "electrum_ws_addr"
type = "crate::config::ResolvAddr"
doc = "Electrum websocket server 'addr:port' to listen on (default: '0.0.0.0:50003' or 20003 for mainnet, '0.0.0.0:60003' or 30003 for testnet and '0.0.0.0:60403' or 30403 for regtest)"

[[param]]
name = "daemon_rpc_addr"
type = "crate::config::ResolvAddr"
doc = "Bitcoin daemon JSONRPC 'addr:port' to connect (default: 127.0.0.1:8332 or 7227 for mainnet, 127.0.0.1:18332 or 7229 for testnet, 28332 for testnet4 (BCH only), 38332 for scalenet (BCH only), 48332 for chipnet (BCH only) and 127.0.0.1:18443 or 18332 for regtest)"

[[param]]
name = "daemon_p2p_addr"
type = "crate::config::ResolvAddr"
doc = "Bitcoin daemon P2P 'addr:port' to connect to (defaults to 127.0.0.1:<default port for network>)"

[[param]]
name = "daemon_rpc_connections"
type = "u32"
doc = "Number of concurrent connections to the full node's RPC interface. More connections will reduce lock contention (improve performance). Most nodes accept at most 4 by default, if increased above that, then also configure the node to accept more connections."
default = "3"

[[param]]
name = "monitoring_addr"
type = "crate::config::ResolvAddr"
doc = "Prometheus monitoring 'addr:port' to listen on (default: 127.0.0.1:4224 or 3224 for mainnet, 127.0.0.1:14224 or 13224 for testnet, 127.0.0.1:34224 or 23224 for testnet4, 127.0.0.1:44224 for scalenet 127.0.0.1:54224 for chipnet and 127.0.0.1:24224 for regtest)"

[[param]]
name = "wait_duration_secs"
type = "u64"
doc = "Duration to wait between bitcoind polling"
default = "5"

[[param]]
name = "index_batch_size"
type = "usize"
doc = "Buffer size for blocks (# of blocks) fetched via RPC from bitcoind"
default = "100"

[[param]]
name = "tx_cache_size_mb"
type = "f32"
doc = "Total size of transactions to cache (MB)"
default = "250.0"

[[param]]
name = "blocktxids_cache_size_mb"
type = "f32"
doc = "Total size of block transactions IDs to cache (in MB)"
default = "50.0"

[[param]]
name = "txid_limit"
type = "usize"
doc = "DEPRECATED. Not used. Number of transactions to lookup before returning an error."
default = "0"

[[param]]
name = "server_banner"
type = "String"
doc = "The banner to be shown in the Electrum console"
default = """concat!(\"Welcome to Rostrum \", env!(\"CARGO_PKG_VERSION\"), \" (Electrum Rust Server)!\").to_owned()"""


[[param]]
name = "rpc_timeout"
type = "usize"
doc = "Maximum burst time in seconds RPC calls may make. Uses leaky bucket algorithm which refills 0.5 sec per second."
default = "10"

[[switch]]
name = "low_memory"
doc = "Indicate preference to less memory usage over performance"
default = false

[[param]]
name = "rpc_buffer_size"
type = "usize"
doc = "Size of the message queue for each peer. If set too small, subscription notifications may drop"
default = "500"

[[param]]
name = "scripthash_subscription_limit"
type = "u32"
doc = "The maximum number of scripthash subscriptions per connection"
default = "250000"

[[param]]
name = "scripthash_alias_bytes_limit"
type = "u32"
doc = "The maximum number of bytes stored for scripthash aliases. A bitcoincash address alias is 54 bytes, making the default allow ~1800 blockchain.address subscriptions."
default = "100000"

[[param]]
name = "rpc_max_connections"
type = "u32"
doc = "Maximum number of simultaneous RPC connections."
default = "2000"

[[param]]
name = "rpc_max_connections_shared_prefix"
type = "u32"
doc = "Maximum number of simultaneous RPC connections from IP's sharing first two octets (255.255.0.0 for IPv4)."
default = "500"

[[param]]
name = "donation_address"
type = "String"
doc = "Server donation address"

[[switch]]
name = "version"
doc = "Print rostrum version and exit"
default = false

[[switch]]
name = "reindex"
doc = "Rebuild full rostrum index at startup"
default = false

[[param]]
name = "reindex_last_blocks"
type = "usize"
doc = "Number of last blocks to reindex (used for testing)"
default = "0"

[[param]]
name = "announce_ssl_port"
type = "u16"
doc = "For peer discovery. If this server is set up with SSL support, set this argument to announce it to other servers. (Example: 50002 for BCH mainnet)"
default = "0"

[[param]]
name = "announce_wss_port"
type = "u16"
doc = "For peer discovery. If this server set up with WebSocket SSL support, set this argument to announce it to other servers. (Example: 50004 for BCH mainnet)"
default = "0"

[[param]]
name = "announce_hostname"
type = "String"
doc = "For peer discovery. If this server has a hostname that resolves to its IP, set this to announce it to other servers. (Example: rostrum.bitcoincash.network). If this is not set and announcements are enabled, then rostrum will attempt to identify it's own public IP."
default = "String::default()"

[[switch]]
name = "announce"
doc = "Enable peer discovery. This server will connect to other electrum servers to collect peers and also announce its own existance. Default on if electrum-rpc-addr is not set or set to a globally routable IP. Disabled if electrum-rpc-addr is set to a local IP."
default = true

[[switch]]
name = "websocket"
doc = "Enable websocket server"
default = true

[[switch]]
name = "metrics"
doc = "Enable prometheus metrics server"
default = true

[[param]]
name = "rpc_idle_timeout"
type = "u64"
doc = "Duration, in seconds, to wait before disconnecting a client due to inactivity. If a client remains idle and doesn't send any request within this time frame, it will be automatically disconnected."
default = "200"

[[switch]]
name = "http"
doc = "Enable HTTP interface"
default = false

[[param]]
name = "electrum_http_addr"
type = "crate::config::ResolvAddr"
doc = "Electrum http server 'addr:port' to listen on (default: '0.0.0.0:51000' or 21000 for mainnet, '0.0.0.0:61000' or 31000 for testnet and '0.0.0.0:61400' or 31400 for regtest)"

[[switch]]
name = "tcp"
doc = "Enable TCP interface"
default = true