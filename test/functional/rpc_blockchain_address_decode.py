#!/usr/bin/env python3
# Copyright (c) 2022 The Bitcoin Unlimited developers
"""
Tests for RPC call 'blockchain.address.decode'
"""
import asyncio
import codecs
from test_framework.util import assert_equal
from test_framework.electrumutil import (
    ElectrumTestFramework,
)
from test_framework.electrumconnection import ElectrumConnection
from test_framework import cashaddr


class BlockchainAddressDecode(ElectrumTestFramework):
    async def run_test(self):
        cli = ElectrumConnection()
        await cli.connect()
        dummy_hash = bytes(20)
        p2pkh = cashaddr.encode("bitcoincash", cashaddr.PUBKEY_TYPE, dummy_hash)
        p2pkh_decoded = await cli.call(
            "blockchain.address.decode", f"bitcoincash:{p2pkh}"
        )
        assert_equal("p2pkh", p2pkh_decoded["type"])
        assert_equal(
            codecs.encode(dummy_hash, "hex").decode("ascii"),
            p2pkh_decoded["payload"],
        )
        assert_equal(False, p2pkh_decoded["is_token_aware"])

        p2sh = cashaddr.encode("bitcoincash", cashaddr.SCRIPT_TYPE, dummy_hash)
        p2sh_decoded = await cli.call(
            "blockchain.address.decode", f"bitcoincash:{p2sh}"
        )
        assert_equal("p2sh", p2sh_decoded["type"])
        assert_equal(
            codecs.encode(dummy_hash, "hex").decode("ascii"),
            p2sh_decoded["payload"],
        )
        assert_equal(False, p2sh_decoded["is_token_aware"])
        cli.disconnect()


if __name__ == "__main__":
    asyncio.run(BlockchainAddressDecode().main())
