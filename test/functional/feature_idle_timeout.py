#!/usr/bin/env python3
# Copyright (c) 2023 The Bitcoin Unlimited developers
"""
Test that a idle connection is disconnected
"""
import asyncio
from test_framework.environment import NodeFeature, node, node_supports, testing_http
from test_framework.util import wait_for
from test_framework.electrumutil import ElectrumTestFramework
from test_framework.electrumconnection import ElectrumConnection


class FeatureIdleTimeout(ElectrumTestFramework):
    def __init__(self):
        super().__init__()
        self.extra_args = [["-electrum.rawarg=--rpc-idle-timeout=5"]]

    async def run_test(self):

        if not node_supports(node(), NodeFeature.SPAWN_ROSTRUM):
            # the extra_arg above requires this
            print("skipping test")
            return

        if testing_http():
            print("skipping test")
            return

        cli = ElectrumConnection()
        await cli.connect()

        # timeout is 5 seconds, but may take a while for client to detect
        async def check_connection():
            return not cli.is_connected()

        await wait_for(30, check_connection)


if __name__ == "__main__":
    asyncio.run(FeatureIdleTimeout().main())
