#!/usr/bin/env python3
# Copyright (c) 2023 The Bitcoin Unlimited developers
import asyncio
from test_framework.util import assert_equal
from test_framework.electrumutil import (
    ElectrumTestFramework,
)
from test_framework.electrumconnection import ElectrumConnection


class AddressListunspentTests(ElectrumTestFramework):
    async def run_test(self):
        n = self.nodes[0]

        n.generate(104)

        await self.bootstrap_p2p()
        cli = ElectrumConnection()
        await cli.connect()
        try:
            await self.test_filters(n, cli)

        finally:
            cli.disconnect()

    async def test_filters(self, n, cli):
        # Mine coins for the node wallet
        addr = n.getnewaddress()

        # add unspent token ..
        await self.create_token(mint_amount=1000, to_addr=addr)

        # add unspent without token
        n.sendtoaddress(addr, 100)

        await self.sync_mempool(cli)

        # Default is to include all
        r = await cli.call("blockchain.address.listunspent", addr)
        assert_equal(2, len(r))

        # Explicily include tokens
        r = await cli.call("blockchain.address.listunspent", addr, "include_tokens")
        assert_equal(2, len(r))

        # Explicitly want Tokens only
        r = await cli.call("blockchain.address.listunspent", addr, "tokens_only")
        assert_equal(1, len(r))
        assert_equal(r[0]["has_token"], True)

        # Explicitly exclude tokens
        r = await cli.call("blockchain.address.listunspent", addr, "exclude_tokens")
        assert_equal(1, len(r))
        assert_equal(r[0]["has_token"], False)


if __name__ == "__main__":
    asyncio.run(AddressListunspentTests().main())
