#!/usr/bin/env python3
# Copyright (c) 2023 The Bitcoin Unlimited developers
"""
Tests for RPC call 'blockchain.block.get'
"""
import asyncio
from test_framework.util import assert_equal
from test_framework.electrumutil import (
    ElectrumTestFramework,
)
from test_framework.electrumconnection import ElectrumConnection


class BlockchainBlockGet(ElectrumTestFramework):
    async def run_test(self):
        try:
            cli = ElectrumConnection()
            await cli.connect()

            n = self.nodes[0]
            block_hashes = n.generate(10)

            await self.sync_height(cli)

            # Should accept block hash as parameter
            assert_equal(
                n.getblock(block_hashes[-1], 0),
                await cli.call("blockchain.block.get", block_hashes[-1]),
            )

            # Should accept block height as parameter
            assert_equal(
                n.getblock(block_hashes[-1], 0),
                await cli.call("blockchain.block.get", n.getblockcount()),
            )

        finally:
            cli.disconnect()


if __name__ == "__main__":
    asyncio.run(BlockchainBlockGet().main())
