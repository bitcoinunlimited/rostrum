import asyncio
import json
from typing import Any
import http.client


def request(host, port, data):
    conn = http.client.HTTPConnection(host, port, timeout=10)
    # Convert data to JSON string
    json_data = json.dumps(data)
    headers = {"Content-Type": "application/json"}

    print(f"http: Sending request {host}:{port}", json_data)
    conn.request("POST", "/", body=json_data, headers=headers)
    resp = conn.getresponse()
    return json.loads(resp.read().decode())


async def request_async(loop, host, port, data):
    def send_request():
        return request(host, port, data)

    # Offload the blocking I/O operation to a thread pool
    return await loop.run_in_executor(None, send_request)


# pylint: disable=no-member,protected-access
class HTTPProtocol:
    client: Any = None  # circluar reference, cant define type
    host: str
    loop: asyncio.AbstractEventLoop
    port: int

    def __init__(self, host: str, port: int, client, loop: asyncio.AbstractEventLoop):
        self.client = client
        self.host = host
        self.loop = loop
        self.port = port

    async def connect(self):
        # Send a ping to see if it's possible to connect (throws on connection error)
        request(self.host, self.port, {"method": "server.ping", "params": []})

    async def send_data_task(self, data: dict):
        try:
            response = await request_async(self.loop, self.host, self.port, data)
            self.client._got_response(response)

        except (http.client.HTTPException, ConnectionError) as e:
            print("HTTP request failed:", e)
            self.client.connection_lost(e)
        except Exception as e:
            print("Unexpected error occurred:", e)
            self.client.connection_lost(e)

    def send_data(self, data: dict):
        self.loop.create_task(self.send_data_task(data))

    def close(self):
        pass
