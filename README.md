# Rostrum - Electrum Server in Rust

An efficient implementation of Electrum Server for Bitcoin Cash and Nexa with token support.

Rostrum supports BCHUnlimited, Nexa and BCHN and has a large test set to ensure these full nodes
always work with the software.

![logo](https://gitlab.com/nexa/rostrum/-/raw/master/contrib/images/logo-platform-256x256.png)

### See [Documentation](https://nexa.gitlab.io/rostrum/)
### See [Protocol Methods](https://nexa.gitlab.io/rostrum/protocol/methods)
### See [Downloads](https://nexa.gitlab.io/rostrum/install/)

## Project

Rostrum is an efficient implementation of Electrum Server and can be used
as a drop-in replacement for ElectrumX. In addition to the TCP RPC interface,
it also provides WebSocket support.

Rostrum fully implements the [Electrum v1.4.3 protocol](https://electrumx-spesmilo.readthedocs.io/en/latest/protocol.html)
plus many more queries, including full token support. See this projects documentation for full RPC reference.

The server indexes the entire blockchain, and the resulting index
enables fast queries for blockchain applications and any given user wallet,
allowing the user to keep real-time track of his balances and his transaction
history.

When run on the user's own machine, there is no need for the wallet to
communicate with external Electrum servers,
thus preserving the privacy of the user's addresses and balances.

## Usage

See [here](https://nexa.gitlab.io/rostrum/usage/) for installation, build and usage instructions.

## Tests

Run unit tests with `cargo test`.

Rostrum has good integration test coverage with Nexa and Bitcoin Unlimited full node. See the [testing documentation](
https://nexa.gitlab.io/rostrum/tests/)

To run functional tests against node software, run `./test/functional/test_runner.py`.
