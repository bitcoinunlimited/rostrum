site_name: Rostrum Documentation
site_description: Documentation for Rostrum - Electrum Server in Rust
site_author: Bitcoin Unlimited

repo_url: https://gitlab.com/nexa/rostrum
docs_dir: doc
site_dir: build-docs

theme:
  name: material
  palette:
    scheme: slate

markdown_extensions:
  - pymdownx.snippets
  - pymdownx.tabbed:
      alternate_style: true

plugins:
  - site-urls

nav:
  - Home: index.md
  - Protocol Reference:
    - protocol/basics.md
    - protocol/methods.md
    - protocol/discovery.md
    - RPC Methods:
      - blockchain.address.decode: protocol/blockchain/blockchain-address-decode.md
      - blockchain.address.get_balance: protocol/blockchain/blockchain-address-get_balance.md
      - blockchain.address.get_first_use: protocol/blockchain/blockchain-address-get_first_use.md
      - blockchain.address.get_history: protocol/blockchain/blockchain-address-get_history.md
      - blockchain.address.get_mempool: protocol/blockchain/blockchain-address-get_mempool.md
      - blockchain.address.get_scripthash: protocol/blockchain/blockchain-address-get_scripthash.md
      - blockchain.address.get_volume: protocol/blockchain/blockchain-address-get_volume.md
      - blockchain.address.listunspent: protocol/blockchain/blockchain-address-listunspent.md
      - blockchain.address.subscribe: protocol/blockchain/blockchain-address-subscribe.md
      - blockchain.address.unsubscribe: protocol/blockchain/blockchain-address-unsubscribe.md
      - blockchain.block.get: protocol/blockchain/blockchain-block-get.md
      - blockchain.block.header: protocol/blockchain/blockchain-block-header.md
      - blockchain.block.header_verbose: protocol/blockchain/blockchain-block-header_verbose.md
      - blockchain.block.headers: protocol/blockchain/blockchain-block-headers.md
      - blockchain.estimatefee: protocol/blockchain/blockchain-estimatefee.md
      - blockchain.headers.subscribe: protocol/blockchain/blockchain-headers-subscribe.md
      - blockchain.headers.tip: protocol/blockchain/blockchain-headers-tip.md
      - blockchain.relayfee: protocol/blockchain/blockchain-relayfee.md
      - blockchain.scripthash.get_balance: protocol/blockchain/blockchain-scripthash-get_balance.md
      - blockchain.scripthash.get_first_use: protocol/blockchain/blockchain-scripthash-get_first_use.md
      - blockchain.scripthash.get_history: protocol/blockchain/blockchain-scripthash-get_history.md
      - blockchain.scripthash.get_mempool: protocol/blockchain/blockchain-scripthash-get_mempool.md
      - blockchain.scripthash.get_volume: protocol/blockchain/blockchain-scripthash-get_volume.md
      - blockchain.scripthash.listunspent: protocol/blockchain/blockchain-scripthash-listunspent.md
      - blockchain.scripthash.subscribe: protocol/blockchain/blockchain-scripthash-subscribe.md
      - blockchain.scripthash.unsubscribe: protocol/blockchain/blockchain-scripthash-unsubscribe.md
      - blockchain.transaction.broadcast: protocol/blockchain/blockchain-transaction-broadcast.md
      - blockchain.transaction.get: protocol/blockchain/blockchain-transaction-get.md
      - blockchain.transaction.get_confirmed_blockhash: protocol/blockchain/blockchain-transaction-get_confirmed_blockhash.md
      - blockchain.transaction.get_merkle: protocol/blockchain/blockchain-transaction-get_merkle.md
      - blockchain.transaction.id_from_pos: protocol/blockchain/blockchain-transaction-id_from_pos.md
      - blockchain.utxo.get: protocol/blockchain/blockchain-utxo-get.md
      - mempool.count: protocol/mempool/mempool-count.md
      - mempool.get: protocol/mempool/mempool-get.md
      - mempool.get_fee_histogram: protocol/mempool/mempool-get_fee_histogram.md
      - server.add_peer: protocol/server/server-add_peer.md
      - server.banner: protocol/server/server-banner.md
      - server.donation_address: protocol/server/server-donation_address.md
      - server.features: protocol/server/server-features.md
      - server.peers.subscribe: protocol/server/server-peers-subscribe.md
      - server.ping: protocol/server/server-ping.md
      - server.version: protocol/server/server-version.md
      - token.address.get_balance: protocol/token/token-address-get_balance.md
      - token.address.get_history: protocol/token/token-address-get_history.md
      - token.address.get_mempool: protocol/token/token-address-get_mempool.md
      - token.address.listunspent: protocol/token/token-address-listunspent.md
      - token.genesis.info: protocol/token/token-genesis-info.md
      - token.nft.list: protocol/token/token-nft-list.md
      - token.scripthash.get_balance: protocol/token/token-scripthash-get_balance.md
      - token.scripthash.get_history: protocol/token/token-scripthash-get_history.md
      - token.scripthash.get_mempool: protocol/token/token-scripthash-get_mempool.md
      - token.scripthash.listunspent: protocol/token/token-scripthash-listunspent.md
      - token.transaction.get_history: protocol/token/token-transaction-get_history.md

  - Usage:
    - usage.md
    - install.md
    - build.md
    - Configuration: configuration.md
    - Full node setup: bitcoind.md
    - Clients & Libraries: connecting.md
    - ssl.md
    - monitoring.md
  - Technical:
    - Ports: ports.md
    - websockets.md
    - httpconnection.md
    - tor.md
    - low-memory.md
    - rpc.md
    - systemd.md
    - cookie_deprecation.md
  - Developers:
    - tests.md
    - schema.md
    - cross-compile.md

